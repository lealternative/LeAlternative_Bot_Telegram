<?php

$regexunshort = '%\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))%i';
$regexshortener = '%(?:https?://)?(?:www\.)?\bbit\.ly\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\btinyurl\.com\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\brebrand\.ly\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bcutt\.ly\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bkutt\.it\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\blnk\.pw\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bow\.ly\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bbuff\.ly\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bt\.co\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bift\.tt\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bt\.ly\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bamzn\.to\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bwp\.me\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bmaglit\.me\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bdub\.sh\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bgoo\.gl\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\btny\.im\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bibit\.ly\b\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bis\.gd\b\/*/(?:(?=\?)|(?:\S*))%i'; // bit.ly/, tinyurl.com, cutt.ly, kutt.it, lnk.pw, ow.ly, buff.ly, t.co, ift.tt, bt.ly, amzn.to, wp.me, maglit.me, goo.gl, is.gd, ibit.ly // 
  
if (($messagefromagroup === FALSE) AND preg_match($regexshortener, $text)) {
  preg_match_all($regexshortener, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 2) as $match) {
  
  preg_match($regexshortener, $text, $matches);  
  

$url = $match[0];
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Must be set to true so that PHP follows any "Location:" header
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$a = curl_exec($ch); // $a will contain all headers

$url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); // This is what you need, it will return you the last effective URL
	$content = array(
		'chat_id' => $chat_id,
	  'text' => 'Ciao, ecco dove porta realmente il tuo link: ' . $url . '',
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}	
	
}

if (($messagefromagroup === TRUE) AND preg_match($regexshortener, $text) AND (empty($editedmessage)) ) {
  preg_match_all($regexshortener, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 2) as $match) { 
  preg_match($regexshortener, $text, $matches);  
  
$url = $match[0];
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Must be set to true so that PHP follows any "Location:" header
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$a = curl_exec($ch); // $a will contain all headers

$url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); // This is what you need, it will return you the last effective URL
	$content = array(
		'chat_id' => $chat_id,
	  'reply_to_message_id' => $message_id,
	  'text' => "<b>Attenzione</b>, hai inserito un link <i>accorciato</i>, per motivi di privacy e sicurezza è sempre bene usare URL interi.\nEcco dove porta realmente il tuo link: $url ",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
	
}

if (($messagefromagroup === FALSE) AND preg_match($regexshortener, $caption)) {
  preg_match_all($regexshortener, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 2) as $match) {
  
  preg_match($regexshortener, $caption, $matches);  
  

$url = $match[0];
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Must be set to true so that PHP follows any "Location:" header
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$a = curl_exec($ch); // $a will contain all headers

$url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); // This is what you need, it will return you the last effective URL
	$content = array(
		'chat_id' => $chat_id,
	  'text' => 'Ciao, ecco dove porta realmente il tuo link: ' . $url . '',
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}		
	
}

if (($messagefromagroup === TRUE) AND preg_match($regexshortener, $caption) AND (empty($editedmessage)) ) {
  preg_match_all($regexshortener, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 2) as $match) { 
  preg_match($regexshortener, $caption, $matches);  
  
$url = $match[0];
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Must be set to true so that PHP follows any "Location:" header
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$a = curl_exec($ch); // $a will contain all headers

$url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL); // This is what you need, it will return you the last effective URL
	$content = array(
		'chat_id' => $chat_id,
	  'reply_to_message_id' => $message_id,
	  'text' => "<b>Attenzione</b>, hai inserito un link <i>accorciato</i>, per motivi di privacy e sicurezza è sempre bene usare URL interi.\nEcco dove porta realmente il tuo link: $url ",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
		
}

?>