<?php
 error_reporting(0); 
include("Telegram.php");
include("datisensibili.php");

// Set the bot TOKEN

// Instances the class
$telegram = new Telegram($bot_id);

date_default_timezone_set("Europe/Rome"); 

$skaid = 450540569;
$krustyid = 1669568455;
$vegaid = 34178519;

$lealternativegruppo = -1001244432641;
$lealternativechat = -1001373077466;

$data = $telegram->getData();
$query = $data['inline_query']['query'];
$user = $data['inline_query']['from'];
$queryid = $data['inline_query']['id'];
$editedmessage = $data['edited_message'];
$inline_query_id  	= $telegram->Inline_Query_ID();
$inline_query_text  = $telegram->Inline_Query_Text();
$msgType		  	= $telegram->getUpdateType();
$text 				= $telegram->Text();
$caption 			= $telegram->Caption();
$entities   		= $data['message'] ['entities'];
$chat_id			= $telegram->ChatID();
$replyquote			= $data['message'] ['chat'] ['message_id'];
$message_id			= $telegram->MessageID();
$replytomessage_id	= $telegram->ReplyToMessageID();
$name				= $telegram->FirstName();
$username			= $telegram->Username();
$replymessagetext	= $data['message'] ['reply_to_message'] ['text'];
$replymessagecaption	= $data['message'] ['reply_to_message'] ['caption'];
$userID				= $telegram->UserID();
$grouptitle			= $telegram->messageFromGroupTitle();
$usernamegroup		= $data['message'] ['chat'] ['username'];
$usernamereply		= $data['message'] ['reply_to_message'] ['from'] ['username'];
$replymessageid		= $data['message'] ['reply_to_message'] ['message_id'];
$callback_query = $data['callback_query'];
$callback_query_data = $callback_query['data'];
$callback_query_id = $callback_query['id'];
$messagefromagroup  = $telegram->messageFromGroup();
$regexyt = '%((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube(-nocookie)?\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?%i';
$regextwitter = '%(?:https?://)?(?:www\.)?twitter\.com\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:mobile\.)?twitter\.com\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:mobile\.)?^x\.com\/*/(?:(?=\?)|(?:\S*))%i';
$regextwimg	 = '%(?:https?://)?(?:www\.)?twimg\.com\/*/(?:(?=\?)|(?:\S*))%i';
$regexreddit = '%(?:https?://)?(?:www\.)?reddit\.com\/*/(?:(?=\?)|(?:\S*))|(?:https?://)?(?:www\.)?\bredd\.it\b\/*/(?:(?=\?)|(?:\S*))%i';
$regexmedium = '%(?:https?://)?(?:www\.)?medium\.com\/*/(?:(?=\?)|(?:\S*))%i';
$regeximgur = '%(?:https?://)?(?:www\.)?imgur\.com\/*/(?:(?=\?)|(?:\S*))%i';
$frasicercabot = [
  "\n<b>Avviami in privato o aggiungimi ad un gruppo per altre funzioni!</b>",
"",
  "\n<b>Avviami in privato o aggiungimi ad un gruppo per altre funzioni!</b>",

];
$targetchat         = -1001244432641;
$buttons = [ 
    //First row
    [
        $telegram->buildInlineKeyBoardButton("🧡 Codice sorgente", $url="https://codeberg.org/lealternative/LeAlternative_Bot_Telegram"),
		$telegram->buildInlineKeyBoardButton("❤️ Donazioni", $url="https://www.lealternative.net/donazioni/")

    ]	
];
$buttonsistruzioni = [ 
    //First row
    [
        $telegram->buildInlineKeyBoardButton("📢 Canale", $url="https://t.me/lealternative"), 
        $telegram->buildInlineKeyBoardButton("🍃 Fresh", $url="https://t.me/lealternativefresh"),		
	    $telegram->buildInlineKeyBoardButton("🔗 Sito", $url="https://www.lealternative.net")
    ], 
    //Second row 
    [
        $telegram->buildInlineKeyBoardButton("💬 Gruppo ufficiale", $url="https://t.me/LeAlternativeGruppoUfficiale"),
        $telegram->buildInlineKeyBoardButton("🥸 Chat OT in tempo reale", $url="https://t.me/LeAlternativeChat")
    ],
    [
        $telegram->buildInlineKeyBoardButton("📜 À la carte", $url="https://lista.lealternative.net"),
		$telegram->buildInlineKeyBoardButton("🤖 Questo bot", $url="https://codeberg.org/lealternative/LeAlternative_Bot_Telegram"),
		$telegram->buildInlineKeyBoardButton("❤️ Dona", $url="https://www.lealternative.net/donazioni/")
    ]	
];

// STATUS //

echo "<p>Status: OK</p>";

// END OF STATUS //

// START //

if (($messagefromagroup === FALSE) AND $text == '/start')
	{
	$content = array(
		'chat_id' => $chat_id,
		'reply_markup' => $telegram->buildInlineKeyBoard($buttons),
	  'text' => "<b>Ciao,</b>\nquesto bot può aiutarti nel magico mondo delle alternative. Con me puoi:\n\n• cercare alternative rapide inline;\n• cercare direttamente sul sito LeAlternative.net;\n• reindirizzare automaticamente link su front-end alternativi sia in privato che all'interno di gruppi:\n• scoprire cosa si cela dietro gli URL <i>accorciati</i> come bit.ly!\n\nScrivi /istruzioni e scopri tutte le sue funzionalità oppure scrivi /alternative per scoprire chi siamo e tutto quello che facciamo\n\nScrivi /privacy invece per leggere la privacy policy.",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
	
// END //

// PRIVACY POLICY //

if (($messagefromagroup === FALSE) AND $text == '/privacy')
	{
	$content = array(
		'chat_id' => $chat_id,
	  'text' => "<b>Privacy Policy</b>\n\nPuoi utilizzare il nostro servizio senza fornire alcuna informazione personale, <b>solo in caso di errore</b> saranno raccolti alcuni dati per il corretto funzionamento del bot, quali: ID dell'account, nome utente e regione di appartenenza.\nQuesti dati sono inseriti in fase di registrazione su Telegram e sono pubblici, senza questi dati non potremmo memorizzare le tue informazioni.\nGarantiamo che le tue informazioni personali, in nessuna circostanza, saranno vendute e/o scambiate.\n\n<b>Termini di servizio</b>\n\nIl bot è offerto in modo gratuito e in quanto tale non è garantito l'uptime, pur impegnandoci ad offrire il miglior servizio possibile.\nCi riserviamo il diritto di apportare modifiche a questa pagina in qualunque momento e senza alcun preavviso.\nScrivi /start per iniziare!",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
	
// END //

// RICERCA

// PRIVACY POLICY //

if (($messagefromagroup === FALSE) AND $text == '/privacy')
	{
	$content = array(
		'chat_id' => $chat_id,
	  'text' => "<b>Privacy Policy</b>\n\nPuoi utilizzare il nostro servizio senza fornire alcuna informazione personale, <b>solo in caso di errore</b> saranno raccolti alcuni dati per il corretto funzionamento del bot, quali: ID dell'account, nome utente e regione di appartenenza.\nQuesti dati sono inseriti in fase di registrazione su Telegram e sono pubblici, senza questi dati non potremmo memorizzare le tue informazioni.\nGarantiamo che le tue informazioni personali, in nessuna circostanza, saranno vendute e/o scambiate.\n\n<b>Termini di servizio</b>\n\nIl bot è offerto in modo gratuito e in quanto tale non è garantito l'uptime, pur impegnandoci ad offrire il miglior servizio possibile.\nCi riserviamo il diritto di apportare modifiche a questa pagina in qualunque momento e senza alcun preavviso.\nScrivi /start per iniziare!",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
	
// END //

// Gestione delle inline query
if (isset($data['inline_query'])) {
    $inline_query_id = $data['inline_query']['id'];
    $query = trim($data['inline_query']['query']);

    // Se il testo inizia con "/cerca " o "cerca ", rimuove il prefisso
    if (stripos($query, '/cerca ') === 0) {
        $query = substr($query, strlen('/cerca '));
    } elseif (stripos($query, 'cerca ') === 0) {
        $query = substr($query, strlen('cerca '));
    }

    // Se la query è vuota, invia un messaggio di prompt
    if ($query === '') {
        $results = [
            [
                'type' => 'article',
                'id' => '0',
                'title' => 'Inserisci una query di ricerca',
                'description' => 'Scrivi "cerca [termine]"',
                'input_message_content' => [
                    'message_text' => 'Per cercare, digita "/cerca [termine]"'
                ]
            ]
        ];
        $telegram->answerInlineQuery([
            'inline_query_id' => $inline_query_id,
            'results' => json_encode($results),
            'cache_time' => 0
        ]);
        exit;
    }

    // Prepara la query per l'URL
    $searchquery = str_replace('/', '%252F', $query);
    $searchqueryok = rawurlencode($searchquery);

    // Effettua la richiesta al sito WordPress
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://www.lealternative.net/wp-json/wp/v2/search?search=' . $searchqueryok,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array("cache-control: no-cache")
    ));
    $response = curl_exec($curl);
    curl_close($curl);

    $response = json_decode($response, true);
    $results = [];

    // Se la risposta non è vuota, costruisce i risultati inline
    if (!empty($response)) {
		foreach ($response as $item) {
			$decodedTitle = html_entity_decode($item['title'], ENT_QUOTES, 'UTF-8');
			$decodedExcerpt = isset($item['excerpt']) ? substr(strip_tags(html_entity_decode($item['excerpt'], ENT_QUOTES, 'UTF-8')), 0, 80) : '';
			
			$results[] = [
				'type' => 'article',
				'id' => md5($item['url']),
				'title' => $decodedTitle,
				'description' => $decodedExcerpt,
				'input_message_content' => [
					'message_text' => '<b>' . $decodedTitle . '</b>' . "\n" . $item['url'],
					'parse_mode' => 'HTML'
				],
				'url' => $item['url'],
				'hide_url' => false
			];
		}

    } else {
        // Se non ci sono risultati, restituisce un articolo che lo indica
        $results[] = [
            'type' => 'article',
            'id' => '0',
            'title' => 'Nessun risultato trovato',
            'description' => 'Modifica la tua ricerca',
            'input_message_content' => [
                'message_text' => 'Nessun risultato trovato per: ' . htmlspecialchars($query)
            ]
        ];
    }

    // Risponde all'inline query con i risultati
    $telegram->answerInlineQuery([
        'inline_query_id' => $inline_query_id,
        'results' => json_encode($results),
        'cache_time' => 0
    ]);
    exit;
}
	
// END RICERCA //

// COMANDI SEMPLICI //

if (($messagefromagroup === FALSE) AND $text == '/alternative')
	{
$telegram->sendMessage([
    'chat_id' => $chat_id, 
    'reply_markup' => $telegram->buildInlineKeyBoard($buttonsistruzioni), 
    'text' => 'Qui puoi trovare alcune delle cose che facciamo!'
]);
	}
	
// END COMANDI SEMPLICI //

// OT //

$regexot = '%^[/.!]\bot\b%i';

if (($userID == $skaid || $userID == $krustyid || $userID == $vegaid) AND preg_match($regexot,$text) )
	{
	$content = array(
		'chat_id' => $chat_id,
 'reply_to_message_id' => $replymessageid,
	  'text' => "<b>Attenzione:</b> per modding, OT e vari tecnicismi c'è il gruppo @LeAlternativeChat (senza i 5 minuti di pausa tra un messaggio e l'altro) per non rendere questo troppo complesso. Se volete potete continuare pure il discorso lì, grazie!",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}

// END OT //

// DELOT //

$regexdelot = '%^[/.!]\bdelot\b%i';

if (($userID == $skaid || $userID == $krustyid || $userID == $vegaid) AND preg_match($regexdelot,$text) )
	{
	
	$content4 = array(
		'chat_id' => $targetchat,
		'text' => 'Il messaggio qui sotto è stato scritto da ' . $usernamereply . ' su @LeAlternativeGruppoUfficiale. Visto che era OT è stato inoltrato automaticamente 👇',
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content4);
	
	$content = array(
		'chat_id' => $targetchat,
		'from_chat_id' => $chat_id,
		'message_id' => $replymessageid,
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->forwardMessage($content);	
	
	$content2 = array(
		'chat_id' => $chat_id,
		'text' => 'Ciao ' . $usernamereply . ', il tuo messaggio è stato cancellato e inoltrato automaticamente su @LeAlternativeChat. Entra pure in quel gruppo per parlarne senza continuare la discussione qui, grazie!',
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content2);
	
	$content3 = array(
		'chat_id' => $chat_id,
		'message_id' => $replymessageid
	);
	$telegram->deleteMessage($content3);
	
}

// DELOT //

// FRASI INLINE //

include("frasi.php");

// END FRASI INLINE //

// ISTRUZIONI //

include("istruzioni.php");

// REDIRECT //

include("redirect.php");

// END REDIRECT //

// CODEBERG UTILIZZABILE SOLO DA SKA //

include("codeberg.php");

// END CODEBERG //

// UNSHORT LINK //

include("unshort.php");

// END UNSHORT LINK //

// TRADUCI //

include("traduci.php");

// END TRADUCI //

// MOTORI DI RICERCA //

include("motoridiricerca.php");

// MOTORI DI RICERCA //
	
?>