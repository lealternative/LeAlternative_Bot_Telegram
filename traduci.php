<?php

$lingvainstances = [
"lingva.ml",
"translate.alefvanoon.xyz",
"translate.igna.rocks",
"lingva.pussthecat.org",
"translate.datatunnel.xyz",
];

$regextrad = '%^[/.!]\btrad \b%i';

if(preg_match($regextrad,$text))  {
	
$searchquery = substr($text, 6);

$search  = array('/');
$replace = array('%252F');
$searchquery = str_replace($search, $replace, $searchquery);
$searchqueryok = rawurlencode($searchquery);

	
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://' . $lingvainstances[rand(0,count($lingvainstances)-1)] . '/api/v1/auto/it/' . $searchqueryok . '',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);		

$response = json_decode($response, true); //because of true, it's in an array
		
$telegram->sendMessage([
    'chat_id' => $chat_id,
	"parse_mode" => "HTML",
	'disable_web_page_preview' => true,
    'text' => '<b>Traduzione:</b> ' . preg_replace('/%252F/i','/', $response['translation']) .'

<i>Questa traduzione è merito di una delle <a href="https://github.com/TheDavidDelta/lingva-translate">istanze pubbliche</a> di Lingva, un front-end alternativo per Google Translate.</i>'
]);
	}
$regextraduci = '%^[/.!]\btrad\b$%i';
	
if (($messagefromagroup === TRUE) AND preg_match($regextraduci, $text)) {
	
$search  = array('/');
$replace = array('%252F');
$replymessagetext = str_replace($search, $replace, $replymessagetext);
$replymessagetextok = rawurlencode($replymessagetext);

$replymessagecaption = str_replace($search, $replace, $replymessagecaption);
$replymessagecaptionok = rawurlencode($replymessagecaption);

if (empty($replymessagetext)) {
	
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://' . $lingvainstances[rand(0,count($lingvainstances)-1)] . '/api/v1/auto/it/' . $replymessagecaptionok . '',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);		

$response = json_decode($response, true); //because of true, it's in an array
		
$telegram->sendMessage([
    'chat_id' => $chat_id,
	 'reply_to_message_id' => $replymessageid,	
	"parse_mode" => "HTML",
	'disable_web_page_preview' => true,
    'text' => '<b>Traduzione:</b> ' . preg_replace('/%252F/i','/', $response['translation']) .'

<i>Questa traduzione è merito di una delle <a href="https://github.com/TheDavidDelta/lingva-translate">istanze pubbliche</a> di Lingva, un front-end alternativo per Google Translate.</i>'
]);
	}
	
else {
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://' . $lingvainstances[rand(0,count($lingvainstances)-1)] . '/api/v1/auto/it/' . $replymessagetextok . '',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);		

$response = json_decode($response, true); //because of true, it's in an array
		
$telegram->sendMessage([
    'chat_id' => $chat_id,
	 'reply_to_message_id' => $replymessageid,	
	"parse_mode" => "HTML",
	'disable_web_page_preview' => true,
    'text' => '<b>Traduzione:</b> ' . preg_replace('/%252F/i','/', $response['translation']) .'

<i>Questa traduzione è merito di una delle <a href="https://github.com/TheDavidDelta/lingva-translate">istanze pubbliche</a> di Lingva, un front-end alternativo per Google Translate.</i>'
]);
	}
	
}
?>