<?php

// CODEBERG //

$regexcodebergbug = '%^[/.!]\bbug \b%i';
$regexcodebergnew = '%^[/.!]\bnew \b%i';
$regexcodebergcarte = '%^[/.!]\bcarte \b%i';
$regexcodebergbot = '%^[/.!]\bbot \b%i';

$replymessagetextok = preg_replace( array('/"/i', '/%/i'), array('\"', '%'), $replymessagetext );

if (($userID == $skaid || $userID == $krustyid) AND preg_match($regexcodebergbug,$text)) {
$titolopronto = substr($text, 4);
$codeberg = '{
"assignee": "lealternative",
"body": "Questo post è creato automaticamente da un <a href=\"https://codeberg.org/lealternative/LeAlternative_Bot_Telegram\" target=\"_blank\">bot</a>. Questo messaggio <a href=\"https://t.me/' . $usernamegroup . '/' . $replymessageid . '\" target=\"_blank\">è stato scritto</a> da <b>' . $usernamereply . '</b> sul gruppo Telegram chiamato <b><a href=\"https:/t.me/' . $usernamegroup . '/\" target=\"_blank\">' . $grouptitle . '</a></b>.<br />Qui di seguito il messaggio:<br /><br />' . $replymessagetextok . '",
"closed": false,
"labels": [ 25612, 30191 ],
"milestone": 0,
"title": "' . $titolopronto . '"
}';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://codeberg.org/api/v1/repos/lealternative/LeAlternative/issues?access_token=$codebergapi");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: application/json','Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $codeberg );
$response = curl_exec($ch);
curl_close($ch);

echo $response;
  
$content = array(
		'chat_id' => $chat_id,
		'reply_to_message_id' => $replymessageid,		
		'text' => "Ok, l'errore è stato segnalato in automatico su <b><a href=\"https://codeberg.org/lealternative/LeAlternative/issues\">Codeberg</a></b>, grazie!",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
  
}

if (($userID == $skaid || $userID == $krustyid) AND preg_match($regexcodebergnew,$text)) {
$titolopronto = substr($text, 4);
$codeberg = '{
"assignee": "lealternative",
"body": "Questo post è creato automaticamente da un <a href=\"https://codeberg.org/lealternative/LeAlternative_Bot_Telegram\" target=\"_blank\">bot</a>. Questo messaggio <a href=\"https://t.me/' . $usernamegroup . '/' . $replymessageid . '\" target=\"_blank\">è stato scritto</a> da <b>' . $usernamereply . '</b> sul gruppo Telegram chiamato <b><a href=\"https:/t.me/' . $usernamegroup . '/\" target=\"_blank\">' . $grouptitle . '</a></b>.<br />Qui di seguito il messaggio:<br /><br />' . $replymessagetextok . '",
"closed": false,
"labels": [ 25613, 30191 ],
"milestone": 0,
"title": "' . $titolopronto . '"
}';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://codeberg.org/api/v1/repos/lealternative/LeAlternative/issues?access_token=$codebergapi");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: application/json','Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $codeberg );
$response = curl_exec($ch);
curl_close($ch);

echo $response;
  
$content = array(
		'chat_id' => $chat_id,
		'reply_to_message_id' => $replymessageid,		
		'text' => "Ok, il suggerimento è stato segnalato in automatico su <b><a href=\"https://codeberg.org/lealternative/LeAlternative/issues\">Codeberg</a></b>, grazie!",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
  
}

if (($userID == $skaid || $userID == $krustyid) AND preg_match($regexcodebergcarte,$text)) {
$titolopronto = substr($text, 6);
$codeberg = '{
"assignee": "lealternative",
"body": "Questo post è creato automaticamente da un <a href=\"https://codeberg.org/lealternative/LeAlternative_Bot_Telegram\" target=\"_blank\">bot</a>. Questo messaggio <a href=\"https://t.me/' . $usernamegroup . '/' . $replymessageid . '\" target=\"_blank\">è stato scritto</a> da <b>' . $usernamereply . '</b> sul gruppo Telegram chiamato <b><a href=\"https:/t.me/' . $usernamegroup . '/\" target=\"_blank\">' . $grouptitle . '</a></b>.<br />Qui di seguito il messaggio:<br /><br />' . $replymessagetextok . '",
"closed": false,
"labels": [ 30219 ],
"milestone": 0,
"title": "' . $titolopronto . '"
}';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://codeberg.org/api/v1/repos/lealternative/pages/issues?access_token=$codebergapi");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: application/json','Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $codeberg );
$response = curl_exec($ch);
curl_close($ch);

echo $response;
  
$content = array(
		'chat_id' => $chat_id,
		'reply_to_message_id' => $replymessageid,		
		'text' => "Ok, il suggerimento è stato segnalato in automatico su <b><a href=\"https://codeberg.org/lealternative/pages/issues\">Codeberg</a></b>, grazie!",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
  
}

if (($userID == $skaid || $userID == $krustyid) AND preg_match($regexcodebergbot,$text)) {
$titolopronto = substr($text, 4);
$codeberg = '{
"assignee": "lealternative",
"body": "Questo post è creato automaticamente da un <a href=\"https://codeberg.org/lealternative/LeAlternative_Bot_Telegram\" target=\"_blank\">bot</a>. Questo messaggio <a href=\"https://t.me/' . $usernamegroup . '/' . $replymessageid . '\" target=\"_blank\">è stato scritto</a> da <b>' . $usernamereply . '</b> sul gruppo Telegram chiamato <b><a href=\"https:/t.me/' . $usernamegroup . '/\" target=\"_blank\">' . $grouptitle . '</a></b>.<br />Qui di seguito il messaggio:<br /><br />' . $replymessagetextok . '",
"closed": false,
"labels": [ 32137 ],
"milestone": 0,
"title": "' . $titolopronto . '"
}';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://codeberg.org/api/v1/repos/lealternative/LeAlternative_Bot_Telegram/issues?access_token=$codebergapi");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('accept: application/json','Content-Type: application/json'));
curl_setopt($ch, CURLOPT_POSTFIELDS, $codeberg );
$response = curl_exec($ch);
curl_close($ch);

echo $response;
  
$content = array(
		'chat_id' => $chat_id,
		'reply_to_message_id' => $replymessageid,		
		'text' => "Ok, la segnalazione è stata inserita automaticamente su <b><a href=\"https://codeberg.org/lealternative/LeAlternative_Bot_Telegram/issues\">Codeberg</a></b>, grazie!",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
  
}

// END CODEBERG //

?>