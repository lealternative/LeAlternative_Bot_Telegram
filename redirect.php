<?php

// NEL GRUPPO //

$youtuberedirects = [
"piped.kavin.rocks",
"https://invidious.snopyta.org/redirect?referer=",
];

$redditredirects = [
"https://teddit.projectsegfau.lt/",
"teddit.pussthecat.org",
"safereddit.com",
"libreddit.kavin.rocks",
];

$imgurredirects = [
"rimgo.pussthecat.org",
"rimgo.projectsegfau.lt",
];

$githubredirects = [
"gh.bloatcat.tk",
"gothub.frontendfriendly.xyz",
"gothub.projectsegfau.lt",
];

$regextiktok = '%(?x)(?:(?:https?:\/\/)?(?:www|m)\.(?:tiktok.com)\/(?:v|embed|trending|@\w+\/video)(?:\/)?(?:\?shareId=)?)(?P<id>[\da-z]+)%i';
$regextiktokvm = '%(?x)(?:(?:https?:\/\/)?(?:www|m|vm)\.(?:tiktok.com)\/(?:\/)?(?:\?shareId=)?)(?P<id>[\da-z]+)%i';
$regextiktok2 = '%(?x)(?:(?:https?:\/\/)?(?:www|m)\.(?:tiktok.com)\/(?:v|embed|trending|@\w+\/video)(?:\/)?(?:\?shareId=)?)%i';
$regexgithub = '%(?:https?://)?(?:www\.)?github\.com\/*/(?!.*(general-discussion|issues|releases)).*$%i';

if (($messagefromagroup === TRUE) AND preg_match($regextwitter, $text) || preg_match($regextwimg, $text) || preg_match($regextiktok, $text) || preg_match($regextiktokvm, $text) || preg_match($regexyt, $text) || preg_match($regexreddit, $text) || preg_match($regexgithub, $text) || preg_match($regexmedium, $text) AND (empty($editedmessage)) ) {
  
$alltwitter = [];
$alltwimg = [];
$allyoutube = [];
$allreddit = [];
$allgithub = [];
$allmedium = [];
$alltiktok = [];
$alltiktokvm = [];

  preg_match_all($regextiktokvm, $text, $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 5) as $match) { 

  preg_match($regextiktokvm, $text, $matches);
	
  $tiktokokvm = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/vm.tiktok.com\//i'), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
  $tiktokokvm = "• " . $tiktokokvm . "\n";
  
array_push($alltiktokvm, $tiktokokvm);

 }
 
  preg_match_all($regextiktok, $text, $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 5) as $match) { 

  preg_match($regextiktok, $text, $matches);
	
  $tiktokok = preg_replace( array('/http:\/\//i', '/https:\/\//i', $regextiktok2), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
  $tiktokok = "• " . $tiktokok . "\n";
  
array_push($alltiktok, $tiktokok);

 } 
  
  preg_match_all($regextwitter, $text, $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 5) as $match) { 

  preg_match($regextwitter, $text, $matches);
	
  $twitterok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/mobile.twitter.com/i', '/twitter.com/i', '/x.com/i' ), array('', '','twiiit.com', 'twiiit.com', 'twiiit.com'), $match[0] );
  
  $twitterok = "• " . $twitterok . "\n";
  
array_push($alltwitter, $twitterok);

 }
 
  preg_match_all($regextwimg, $text, $matchestwimg, PREG_SET_ORDER);
foreach(array_slice($matchestwimg, 0, 5) as $matchtwimg) { 

  preg_match($regextwimg, $text, $matchestwimg);
	
  $twimgok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/twimg.com/i'), array('', '','twiiit.com/pic'), $matchtwimg[0] );
  
  $twimgok = "• " . $twimgok . "\n";
  
array_push($alltwimg, $twimgok);

 }

  preg_match_all($regexyt, $text, $matchesyt, PREG_SET_ORDER);
  foreach(array_slice($matchesyt, 0, 5) as $matchyt) {  
  preg_match($regexyt, $text, $matchesyt);
  
  $invidiousok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '',$youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)]), $matchyt[0] );
 
 $invidiousok = "• " . $invidiousok . "\n";
 
 array_push($allyoutube, $invidiousok); 
 
}

  preg_match_all($regexreddit, $text, $matchesreddit, PREG_SET_ORDER);
  foreach(array_slice($matchesreddit, 0, 5) as $matchreddit) {
  
  preg_match($regexreddit, $text, $matchesreddit);
 
  $tedditok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www./i', '/old.reddit.com/i', '/reddit.com/i', '/redd.it/i'), array('', '', '', $redditredirects[rand(0,count($redditredirects)-1)], $redditredirects[rand(0,count($redditredirects)-1)], $redditredirects[rand(0,count($redditredirects)-1)]), $matchreddit[0] );
  
  $tedditok = "• " . $tedditok . "\n";
  
 array_push($allreddit, $tedditok); 

}

  preg_match_all($regexgithub, $text, $matchesgithub, PREG_SET_ORDER);
  foreach(array_slice($matchesgithub, 0, 5) as $matchegithub) {
  
  preg_match($regexgithub, $text, $matchesgithub);
 
  $githubok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/github.com/i'), array('', '', $githubredirects[rand(0,count($githubredirects)-1)],), $matchegithub[0] );
  
  $githubok = "• " . $githubok . "\n";
  
 array_push($allmedium, $githubok); 

}

  preg_match_all($regexmedium, $text, $matchesmedium, PREG_SET_ORDER);
  foreach(array_slice($matchesmedium, 0, 5) as $matchmedium) {
  
  preg_match($regexmedium, $text, $matchesmedium);
 
  $mediumok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/medium.com/i'), array('', '','scribe.rip'), $matchmedium[0] );
  
  $mediumok = "• " . $mediumok . "\n";
  
 array_push($allmedium, $mediumok); 

} 
  
$content = array(
		'chat_id' => $chat_id,
		'reply_to_message_id' => $message_id,
		'text' => 'Ciao, sai che il tuo testo contiene alcuni link che potrebbero essere letti su <a href="https://www.lealternative.net/2021/01/07/alternative-per-guardare-i-social-network/">front-end alternativi</a>? Ad esempio:
		
'.$alltwitter = (empty($regextwitter)) ? 'default' : implode($alltwitter).''.$alltwimg = (empty($regextwimg)) ? 'default' : implode($alltwimg).''.$allyoutube = (empty($regexyt)) ? 'default' : implode($allyoutube).''.$allreddit = (empty($regexreddit)) ? 'default' : implode($allreddit).''.$allmedium = (empty($regexmedium)) ? 'default' : implode($allmedium).''.$alltiktok = (empty($regextiktok)) ? 'default' : implode($alltiktok).''.$alltiktokvm = (empty($regextiktokvm)) ? 'default' : implode($alltiktokvm).'',
		'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);		
	
}

// END NEL GRUPPO //

// NEL GRUPPO CAPTION //

if (($messagefromagroup === TRUE) AND preg_match($regextwitter, $caption) || preg_match($regextwimg, $caption) || preg_match($regextiktok, $caption) || preg_match($regextiktokvm, $caption) || preg_match($regexyt, $caption) || preg_match($regexreddit, $caption) || preg_match($regexgithub, $caption) || preg_match($regexmedium, $caption) AND (empty($editedmessage))) {
  
$alltwitter = [];
$alltwimg = [];
$allyoutube = [];
$allreddit = [];
$allgithub = [];
$allmedium = [];
$alltiktok = [];
$alltiktokvm = [];

  preg_match_all($regextiktokvm, $caption, $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 5) as $match) { 

  preg_match($regextiktokvm, $caption, $matches);
	
  $tiktokokvm = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/vm.tiktok.com\//i'), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
  $tiktokokvm = "• " . $tiktokokvm . "\n";
  
array_push($alltiktokvm, $tiktokokvm);

 }

  preg_match_all($regextiktok, $caption, $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 5) as $match) { 

  preg_match($regextiktok, $caption, $matches);
	
  $tiktokok = preg_replace( array('/http:\/\//i', '/https:\/\//i', $regextiktok2), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
  $tiktokok = "• " . $tiktokok . "\n";
  
array_push($alltiktok, $tiktokok);

 }
  
  preg_match_all($regextwitter, $caption, $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 5) as $match) { 

  preg_match($regextwitter, $caption, $matches);
	
  $twitterok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/mobile.twitter.com/i', '/twitter.com/i'), array('', '','twiiit.com', 'twiiit.com'), $match[0] );
  
  $twitterok = "• " . $twitterok . "\n";
  
array_push($alltwitter, $twitterok);

 }
 
  preg_match_all($regextwimg, $caption, $matchestwimg, PREG_SET_ORDER);
foreach(array_slice($matchestwimg, 0, 5) as $matchtwimg) { 

  preg_match($regextwimg, $caption, $matchestwimg);
	
  $twimgok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/twimg.com/i'), array('', '','twiiit.com/pic'), $matchtwimg[0] );
  
  $twimgok = "• " . $twimgok . "\n";
  
array_push($alltwimg, $twimgok);

 }

  preg_match_all($regexyt, $caption, $matchesyt, PREG_SET_ORDER);
  foreach(array_slice($matchesyt, 0, 5) as $matchyt) {  
  preg_match($regexyt, $caption, $matchesyt);
  
  $invidiousok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '',$youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)]), $matchyt[0] );
 
 $invidiousok = "• " . $invidiousok . "\n";
 
 array_push($allyoutube, $invidiousok); 
 
}

  preg_match_all($regexreddit, $caption, $matchesreddit, PREG_SET_ORDER);
  foreach(array_slice($matchesreddit, 0, 5) as $matchreddit) {
  
  preg_match($regexreddit, $caption, $matchesreddit);
 
  $tedditok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www./i', '/old.reddit.com/i', '/reddit.com/i', '/redd.it/i'), array('', '', '', $redditredirects[rand(0,count($redditredirects)-1)], $redditredirects[rand(0,count($redditredirects)-1)], $redditredirects[rand(0,count($redditredirects)-1)]), $matchreddit[0] );
  
  $tedditok = "• " . $tedditok . "\n";
  
 array_push($allreddit, $tedditok); 

}

  preg_match_all($regexgithub, $caption, $matchesgithub, PREG_SET_ORDER);
  foreach(array_slice($matchesgithub, 0, 5) as $matchegithub) {
  
  preg_match($regexgithub, $caption, $matchesgithub);
 
  $githubok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/github.com/i'), array('', '', $githubredirects[rand(0,count($githubredirects)-1)],), $matchegithub[0] );
  
  $githubok = "• " . $githubok . "\n";
  
 array_push($allgithub, $githubok); 

}

  preg_match_all($regexmedium, $caption, $matchesmedium, PREG_SET_ORDER);
  foreach(array_slice($matchesmedium, 0, 5) as $matchmedium) {
  
  preg_match($regexmedium, $caption, $matchesmedium);
 
  $mediumok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/medium.com/i'), array('', '','scribe.rip'), $matchmedium[0] );
  
  $mediumok = "• " . $mediumok . "\n";
  
 array_push($allmedium, $mediumok); 

} 
  
$content = array(
		'chat_id' => $chat_id,
		'reply_to_message_id' => $message_id,
		'text' => 'Ciao, sai che il tuo testo contiene alcuni link che potrebbero essere letti su <a href="https://www.lealternative.net/2021/01/07/alternative-per-guardare-i-social-network/">front-end alternativi</a>? Ad esempio:
		
'.$alltwitter = (empty($regextwitter)) ? 'default' : implode($alltwitter).''.$alltwimg = (empty($regextwimg)) ? 'default' : implode($alltwimg).''.$allyoutube = (empty($regexyt)) ? 'default' : implode($allyoutube).''.$allreddit = (empty($regexreddit)) ? 'default' : implode($allreddit).''.$allmedium = (empty($regexmedium)) ? 'default' : implode($allmedium).''.$allgithub = (empty($regexgithub)) ? 'default' : implode($allgithub).''.$alltiktok = (empty($regextiktok)) ? 'default' : implode($alltiktok).''.$alltiktokvm = (empty($regextiktokvm)) ? 'default' : implode($alltiktokvm).'',
		'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);	
	
}

// END NEL GRUPPO CAPTION //

// NEL GRUPPO ENTITIES //

if (!empty($entities)) {

foreach(array_slice($entities, 0, 3) as $entita) {

if (($messagefromagroup === TRUE) AND preg_match($regextwitter, $entita[url]) || preg_match($regextwimg, $entita[url]) || preg_match($regextiktok, $entita[url]) || preg_match($regextiktokvm, $entita[url]) || preg_match($regexyt, $entita[url]) || preg_match($regexreddit, $entita[url]) || preg_match($regexgithub, $entita[url]) || preg_match($regexmedium, $entita[url]) AND (empty($editedmessage))) {
  
$alltwitter = [];
$alltwimg = [];
$allyoutube = [];
$allreddit = [];
$allgithub = [];
$allmedium = [];
$alltiktok = [];
$alltiktokvm = [];

  preg_match_all($regextiktokvm, $entita[url], $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 2) as $match) { 

  preg_match($regextiktokvm, $entita[url], $matches);
	
$tiktokokvm = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/vm.tiktok.com\//i'), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
  $tiktokokvm = "• " . $tiktokokvm . "\n";
  
array_push($alltiktokvm, $tiktokokvm);

 }

  preg_match_all($regextiktok, $entita[url], $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 2) as $match) { 

  preg_match($regextiktok, $entita[url], $matches);
	
  $tiktokok = preg_replace( array('/http:\/\//i', '/https:\/\//i', $regextiktok2), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
  $tiktokok = "• " . $tiktokok . "\n";
  
array_push($alltiktok, $tiktokok);

 }

  
  preg_match_all($regextwitter, $entita[url], $matches, PREG_SET_ORDER);
foreach(array_slice($matches, 0, 2) as $match) { 

  preg_match($regextwitter, $entita[url], $matches);
	
  $twitterok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/mobile.twitter.com/i', '/twitter.com/i'), array('', '','twiiit.com', 'twiiit.com'), $match[0] );
  
  $twitterok = "• " . $twitterok . "\n";
  
array_push($alltwitter, $twitterok);

 }
 
  preg_match_all($regextwimg, $entita[url], $matchestwimg, PREG_SET_ORDER);
foreach(array_slice($matchestwimg, 0, 5) as $matchtwimg) { 

  preg_match($regextwimg, $entita[url], $matchestwimg);
	
  $twimgok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/twimg.com/i'), array('', '','twiiit.com/pic'), $matchtwimg[0] );
  
  $twimgok = "• " . $twimgok . "\n";
  
array_push($alltwimg, $twimgok);

 }

  preg_match_all($regexyt, $entita[url], $matchesyt, PREG_SET_ORDER);
  foreach(array_slice($matchesyt, 0, 2) as $matchyt) {  
  preg_match($regexyt, $entita[url], $matchesyt);
  
  $invidiousok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '',$youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)], $youtuberedirects[rand(0,count($youtuberedirects)-1)]), $matchyt[0] );
 
 $invidiousok = "• " . $invidiousok . "\n";
 
 array_push($allyoutube, $invidiousok); 
 
}

  preg_match_all($regexreddit, $entita[url], $matchesreddit, PREG_SET_ORDER);
  foreach(array_slice($matchesreddit, 0, 2) as $matchreddit) {
  
  preg_match($regexreddit, $entita[url], $matchesreddit);
 
  $tedditok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www./i', '/old.reddit.com/i', '/reddit.com/i', '/redd.it/i'), array('', '', '', $redditredirects[rand(0,count($redditredirects)-1)], $redditredirects[rand(0,count($redditredirects)-1)], $redditredirects[rand(0,count($redditredirects)-1)]), $matchreddit[0] );
  
  $tedditok = "• " . $tedditok . "\n";
  
 array_push($allreddit, $tedditok); 

}

  preg_match_all($regexgithub, $entita[url], $matchesgithub, PREG_SET_ORDER);
  foreach(array_slice($matchesgithub, 0, 2) as $matchegithub) {
  
  preg_match($regexgithub, $entita[url], $matchesgithub);
 
  $githubok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/github.com/i'), array('', '', $githubredirects[rand(0,count($githubredirects)-1)],), $matchegithub[0] );
  
  $githubok = "• " . $githubok . "\n";
  
 array_push($allgithub, $githubok); 

}

  preg_match_all($regexmedium, $entita[url], $matchesmedium, PREG_SET_ORDER);
  foreach(array_slice($matchesmedium, 0, 2) as $matchmedium) {
  
  preg_match($regexmedium, $entita[url], $matchesmedium);
 
  $mediumok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/medium.com/i'), array('', '','scribe.rip'), $matchmedium[0] );
  
  $mediumok = "• " . $mediumok . "\n";
  
 array_push($allmedium, $mediumok); 

}
  
$content = array(
		'chat_id' => $chat_id,
		'reply_to_message_id' => $message_id,
		'text' => 'Ciao, sai che il tuo testo contiene alcuni link che potrebbero essere letti su <a href="https://www.lealternative.net/2021/01/07/alternative-per-guardare-i-social-network/">front-end alternativi</a>? Ad esempio:
		
'.$alltwitter = (empty($regextwitter)) ? 'default' : implode($alltwitter).''.$alltwimg = (empty($regextwimg)) ? 'default' : implode($alltwimg).''.$allyoutube = (empty($regexyt)) ? 'default' : implode($allyoutube).''.$allreddit = (empty($regexreddit)) ? 'default' : implode($allreddit).''.$allmedium = (empty($regexmedium)) ? 'default' : implode($allmedium).''.$allgithub = (empty($regexgithub)) ? 'default' : implode($allgithub).''.$alltiktok = (empty($regextiktok)) ? 'default' : implode($alltiktok).''.$alltiktokvm = (empty($regextiktokvm)) ? 'default' : implode($alltiktokvm).'',
		'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);

	
}}}

// END NEL GRUPPO ENTITIES //

// INIZIO IN PRIVATO //

// TWITTER //

if (($messagefromagroup === FALSE) AND preg_match($regextwitter, $text)) {
  
  preg_match_all($regextwitter, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {

  preg_match($regextwitter, $text, $matches);	
	
  $twitterok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/mobile.twitter.com/i', '/twitter.com/i'), array('', '','twiiit.com', 'twiiit.com'), $match[0] );
  $twitterok2 = "https://" . $twitterok;
  
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>Twitter</b> su <b>Nitter</b>!\n\n<a href=\"$twitterok2\">Nitter</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

if (($messagefromagroup === FALSE) AND preg_match($regextwimg, $text)) {
  
  preg_match_all($regextwimg, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {

  preg_match($regextwimg, $text, $matches);	
	
  $twitterok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/twimg.com/i'), array('', '','twiiit.com/pic'), $match[0] );
  $twitterok2 = "https://" . $twitterok;
  
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>Twitter</b> su <b>Nitter</b>!\n\n<a href=\"$twitterok2\">Nitter</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}
	
if (($messagefromagroup === FALSE) AND (strcasecmp($text, 'twitter.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un tweet vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// TWITTER CAPTION //

if (($messagefromagroup === FALSE) AND preg_match($regextwitter, $caption)) {
  
  preg_match_all($regextwitter, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {

  preg_match($regextwitter, $caption, $matches);	
	
  $twitterok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/mobile.twitter.com/i', '/twitter.com/i'), array('', '','twiiit.com', 'twiiit.com'), $match[0] );
  $twitterok2 = "https://" . $twitterok;
  
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>Twitter</b> su <b>Nitter</b>!\n\n<a href=\"$twitterok2\">Nitter</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

if (($messagefromagroup === FALSE) AND preg_match($regextwimg, $caption)) {
  
  preg_match_all($regextwimg, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {

  preg_match($regextwimg, $caption, $matches);	
	
  $twitterok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/twimg.com/i'), array('', '','twiiit.com/pic'), $match[0] );
  $twitterok2 = "https://" . $twitterok;
  
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>Twitter</b> su <b>Nitter</b>!\n\n<a href=\"$twitterok2\">Nitter</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}
	
if (($messagefromagroup === FALSE) AND (strcasecmp($caption, 'twitter.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un video <b>YouTube</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}	

// YOUTUBE //
	
if (($messagefromagroup === FALSE) AND preg_match($regexyt, $text)) {
  
  preg_match_all($regexyt, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regexyt, $text, $matches);
  
  $pipedok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be\/watch?v=/i', '/youtu.be/i', '/youtube.com/i'), array('', '', 'piped.kavin.rocks', 'piped.kavin.rocks', 'piped.kavin.rocks', 'piped.kavin.rocks', 'piped.kavin.rocks'), $match[0] );
  $pipedok2 = "https://" . $pipedok;
    
  $invidiousok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '','invidious.snopyta.org', 'invidious.snopyta.org', 'invidious.snopyta.org', 'invidious.snopyta.org'), $match[0] );
  $invidiousok2 = "https://" . $invidiousok;
	
  $invidiousredirectok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '','redirect.invidious.io', 'redirect.invidious.io', 'redirect.invidious.io', 'redirect.invidious.io'), $match[0] );
  $invidiousredirectok2 = "https://" . $invidiousredirectok;
  
  $viewtubeok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '','viewtube.io', 'viewtube.io', 'viewtube.io', 'viewtube.io'), $match[0] );
  $viewtubeok2 = "https://" . $viewtubeok;  
  
  $cloudtubeok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '','tube.cadence.moe', 'tube.cadence.moe', 'tube.cadence.moe', 'tube.cadence.moe'), $match[0] );
  $cloudtubeok2 = "https://" . $cloudtubeok;  
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Perfetto, ora scegli tra uno di questi front-end alternativi per <b>YouTube</b>!\n\n<a href=\"$pipedok2\">Piped</a> | <a href=\"$invidiousok2\">Invidious</a> (<a href=\"$invidiousredirectok2\">altre istanze</a>) | <a href=\"$viewtubeok2\">ViewTube</a> | <a href=\"$cloudtubeok2\">CloudTube</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

if (($messagefromagroup === FALSE) AND (strcasecmp($text, 'youtube.com') == 0) || (strcasecmp($text, 'youtu.be') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un video <b>YouTube</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// YOUTUBE //
	
if (($messagefromagroup === FALSE) AND preg_match($regexyt, $caption)) {
  
  preg_match_all($regexyt, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regexyt, $caption, $matches);
  
  $pipedok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '', 'piped.kavin.rocks', 'piped.kavin.rocks', 'piped.kavin.rocks', 'piped.kavin.rocks'), $match[0] );
  $pipedok2 = "https://" . $pipedok;
    
  $invidiousok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '','invidious.snopyta.org', 'invidious.snopyta.org', 'invidious.snopyta.org', 'invidious.snopyta.org'), $match[0] );
  $invidiousok2 = "https://" . $invidiousok;
	
  $invidiousredirectok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '','redirect.invidious.io', 'redirect.invidious.io', 'redirect.invidious.io', 'redirect.invidious.io'), $match[0] );
  $invidiousredirectok2 = "https://" . $invidiousredirectok;
  
  $viewtubeok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '','viewtube.io', 'viewtube.io', 'viewtube.io', 'viewtube.io'), $match[0] );
  $viewtubeok2 = "https://" . $viewtubeok;  
  
  $cloudtubeok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www.youtube.com/i', '/m.youtube.com/i', '/youtu.be/i', '/youtube.com/i'), array('', '','tube.cadence.moe', 'tube.cadence.moe', 'tube.cadence.moe', 'tube.cadence.moe'), $match[0] );
  $cloudtubeok2 = "https://" . $cloudtubeok;  
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Perfetto, ora scegli tra uno di questi front-end alternativi per <b>YouTube</b>!\n\n<a href=\"$pipedok2\">Piped</a> | <a href=\"$invidiousok2\">Invidious</a> (<a href=\"$invidiousredirectok2\">altre istanze</a>) | <a href=\"$viewtubeok2\">ViewTube</a> | <a href=\"$cloudtubeok2\">CloudTube</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

if (($messagefromagroup === FALSE) AND (strcasecmp($caption, 'youtube.com') == 0) || (strcasecmp($caption, 'youtu.be') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un video <b>YouTube</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}
	
// REDDIT //
	
if (($messagefromagroup === FALSE) AND preg_match($regexreddit, $text)) {
  
  preg_match_all($regexreddit, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regexreddit, $text, $matches);
  
  $tedditok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/old.reddit.com/i', '/reddit.com/i', '/redd.it/i'), array('', '','teddit.net', 'teddit.net', 'teddit.net'), $match[0] );
  $tedditok2 = "https://" . $tedditok;
  
  $libreddok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/www./i', '/old.reddit.com/i', '/reddit.com/i', '/\bredd.it\b/i'), array('', '', '', 'libreddit.spike.codes', 'libreddit.spike.codes', 'libreddit.spike.codes'), $match[0] );
  $libreddok2 = "https://" . $libreddok;
  
  $xedditok = preg_replace( array('/http:\/\//i', '/https:\/\//i',  '/old.reddit.com/i', '/reddit.com/i', '/redd.it/i'), array('', '','xeddit.com', 'xeddit.com', 'xeddit.com'), $match[0] );
  $xedditok2 = "https://" . $xedditok;
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ora scegli tra uno di questi front-end alternativi per <b>Reddit</b>!\n\n<a href=\"$tedditok2\">Teddit</a> | <a href=\"$libreddok2\">Libreddit</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}	

if (($messagefromagroup === FALSE) AND (strcasecmp($text, 'reddit.com') == 0) || (strcasecmp($text, 'redd.it') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un post su <b>Reddit</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// REDDIT //
	
if (($messagefromagroup === FALSE) AND preg_match($regexreddit, $caption)) {
  
  preg_match_all($regexreddit, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regexreddit, $caption, $matches);
  
  $tedditok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/old.reddit.com/i', '/reddit.com/i', '/redd.it/i'), array('', '','teddit.net', 'teddit.net', 'teddit.net'), $match[0] );
  $tedditok2 = "https://" . $tedditok;
  
  $libreddok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/old.reddit.com/i', '/www./i', '/reddit.com/i', '/\bredd.it\b/i'), array('', '', '', 'libreddit.spike.codes', 'libreddit.spike.codes', 'libreddit.spike.codes'), $match[0] );
  $libreddok2 = "https://" . $libreddok;
  
  $xedditok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/old.reddit.com/i', '/reddit.com/i', '/redd.it/i'), array('', '','xeddit.com', 'xeddit.com', 'xeddit.com'), $match[0] );
  $xedditok2 = "https://" . $xedditok;
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ora scegli tra uno di questi front-end alternativi per <b>Reddit</b>!\n\n<a href=\"$tedditok2\">Teddit</a> | <a href=\"$libreddok2\">Libreddit</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}	

if (($messagefromagroup === FALSE) AND (strcasecmp($caption, 'reddit.com') == 0) || (strcasecmp($caption, 'redd.it') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un post su <b>Reddit</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}	
	
// MEDIUM //

if (($messagefromagroup === FALSE) AND preg_match($regexmedium, $text)) {
  
  preg_match_all($regexmedium, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regexmedium, $text, $matches);
  
  $mediumok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/medium.com/i'), array('', '','scribe.rip'), $match[0] );
  $mediumok2 = "https://" . $mediumok;
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>Medium</b> su <b>Scribe</b>!\n\n<a href=\"$mediumok2\">Scribe</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

if (($messagefromagroup === FALSE) AND (strcasecmp($text, 'medium.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un post di <b>Medium</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// MEDIUM //

if (($messagefromagroup === FALSE) AND preg_match($regexmedium, $caption)) {
  
  preg_match_all($regexmedium, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regexmedium, $caption, $matches);
  
  $mediumok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/medium.com/i'), array('', '','scribe.rip'), $match[0] );
  $mediumok2 = "https://" . $mediumok;
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>Medium</b> su <b>Scribe</b>!\n\n<a href=\"$mediumok2\">Scribe</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

if (($messagefromagroup === FALSE) AND (strcasecmp($caption, 'medium.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un post di <b>Medium</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}		

if (($messagefromagroup === FALSE) AND (strcasecmp($text, 'medium.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un post di <b>Medium</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// TIKTOK TEXT //

if (($messagefromagroup === FALSE) AND preg_match($regextiktok, $text)) {
  
  preg_match_all($regextiktok, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regextiktok, $text, $matches);
  
  $tiktokok = preg_replace( array('/http:\/\//i', '/https:\/\//i', $regextiktok2), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>TikTok</b> su <b>ProxiTok</b>!\n\n<a href=\"$tiktokok\">ProxiTok</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

// TIKTOK TEXT VM //

if (($messagefromagroup === FALSE) AND preg_match($regextiktokvm, $text)) {
  
  preg_match_all($regextiktokvm, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regextiktokvm, $text, $matches);
  
  $tiktokokvm = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/vm.tiktok.com\//i'), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>TikTok</b> su <b>ProxiTok</b>!\n\n<a href=\"$tiktokokvm\">ProxiTok</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

// TIKTOK CAPTION //

if (($messagefromagroup === FALSE) AND preg_match($regextiktok, $caption)) {
  
  preg_match_all($regextiktok, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regextiktok, $caption, $matches);
  
  $tiktokok = preg_replace( array('/http:\/\//i', '/https:\/\//i', $regextiktok2), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>TikTok</b> su <b>ProxiTok</b>!\n\n<a href=\"$tiktokok\">ProxiTok</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}

if (($messagefromagroup === FALSE) AND (strcasecmp($caption, 'tiktok.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un post di <b>TikTok</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}		

if (($messagefromagroup === FALSE) AND (strcasecmp($text, 'tiktok.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un post di <b>TikTok</b> vero e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// TIKTOK CAPTION VM //

if (($messagefromagroup === FALSE) AND preg_match($regextiktokvm, $caption)) {
  
  preg_match_all($regextiktokvm, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
  
  preg_match($regextiktokvm, $caption, $matches);
  
  $tiktokokvm = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/vm.tiktok.com\//i'), array('', '','https://proxitok.pussthecat.org/video/'), $match[0] );
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>TikTok</b> su <b>ProxiTok</b>!\n\n<a href=\"$tiktokokvm\">ProxiTok</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}
	
// IMGUR //

if (($messagefromagroup === FALSE) AND preg_match($regeximgur, $text)) {
  
  preg_match_all($regeximgur, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
	
  preg_match($regeximgur, $text, $matches);
	
  $imgurok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/imgur.com/i'), array('', '',$imgurredirects[rand(0,count($imgurredirects)-1)]), $match[0] );
  $imgurok2 = "https://" . $imgurok;
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>Imgur</b> su <b>rimgo</b>!\n\n<a href=\"$imgurok2\">rimgo</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}	
if (($messagefromagroup === FALSE) AND (strcasecmp($text, 'imgur.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un'immagine di <b>Imgur</b> vera e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// IMGUR //

if (($messagefromagroup === FALSE) AND preg_match($regeximgur, $caption)) {
  
  preg_match_all($regeximgur, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
	
  preg_match($regeximgur, $caption, $matches);
	
  $imgurok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/imgur.com/i'), array('', '',$imgurredirects[rand(0,count($imgurredirects)-1)]), $match[0] );
  $imgurok2 = "https://" . $imgurok;
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>Imgur</b> su <b>rimgo</b>!\n\n<a href=\"$imgurok2\">rimgo</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}	
if (($messagefromagroup === FALSE) AND (strcasecmp($caption, 'imgur.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un'immagine di <b>Imgur</b> vera e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// GITHUB //

if (($messagefromagroup === FALSE) AND preg_match($regexgithub, $text)) {
  
  preg_match_all($regexgithub, $text, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
	
  preg_match($regexgithub, $text, $matches);
	
  $githubredirectsok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/github.com/i'), array('', '',$githubredirects[rand(0,count($githubredirects)-1)]), $match[0] );
  $githubredirectsok2 = "https://" . $githubredirectsok;
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>GitHub</b> su <b>GotHub</b>!\n\n<a href=\"$githubredirectsok2\">GotHub</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}	
if (($messagefromagroup === FALSE) AND (strcasecmp($text, 'github.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un codice su <b>GitHub</b> e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

// GITHUB //

if (($messagefromagroup === FALSE) AND preg_match($regexgithub, $caption)) {
  
  preg_match_all($regexgithub, $caption, $matches, PREG_SET_ORDER);
  foreach(array_slice($matches, 0, 5) as $match) {
	
  preg_match($regexgithub, $caption, $matches);
	
  $githubredirectsok = preg_replace( array('/http:\/\//i', '/https:\/\//i', '/github.com/i'), array('', '',$githubredirects[rand(0,count($githubredirects)-1)]), $match[0] );
  $githubredirectsok2 = "https://" . $githubredirectsok;
  
$content = array(
		'chat_id' => $chat_id,
		'text' => "Perfetto, ecco il tuo link alternativo per <b>GitHub</b> su <b>GotHub</b>!\n\n<a href=\"$githubredirectsok2\">rimgo</a>",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
}	
if (($messagefromagroup === FALSE) AND (strcasecmp($caption, 'github.com') == 0)) {
  
$content = array(
		'chat_id' => $chat_id,
  'text' => "Metti il link di un codice su <b>GitHub</b> e non solo il nome del dominio, grazie",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
}

?>