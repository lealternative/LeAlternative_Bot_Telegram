# LeAlternativeBot-Telegram

Codice sorgente del bot su Telegram chiamato [@LeAlternativeBot](https://t.me/LeAlternativeBot).

È un bot che permette di:

- cercare alternative inline direttamente sul sito [LeAlternative.net](https://www.lealternative.net/);
- reindirizzare automaticamente link su front-end alternativi sia in privato che all'interno di gruppi;
- svelare il vero indirizzo dei dei link accorciati (*shorturl*);
- tradurre del testo da istanze casuali di [Lingva](https://github.com/TheDavidDelta/lingva-translate);
- creare tasti per cercare su DuckDuckGo.

<mark>Tutti i comandi sono _case-insensitive_ e si possono usare con ``/``, con ``!`` e anche con il ``.``</mark>

Ha inoltre altre funzioni/possibilità utilizzabili solo ed esclusivamente se ne create uno per conto vostro, ecco quali:

- creare una issue su un repository di Codeberg quotando un messaggio e scrivendo un comando (attuali: ``/new``, ``/carte``, ``/bug`` e ``/bot``). Il messaggio quotato creerà una issue su Codeberg con dentro il messaggio quotato e come titolo inserirà la frase scritta dopo il comando. Esempio: ``/bug`` Trovato un bug sul sito;
- segnalare OT all'interno del gruppo con un messaggio predefinito: ``/ot``;
- spostare automaticamente un messaggio sul canale OT con il comando: ``/delot``

Creato anche grazie all'aiuto di [@selectallfromdual](https://gitea.it/selectallfromdual) ([Sito](https://www.selectallfromdual.com/)), per quel che riguarda la parte inline.

L'idea del redirect automatico viene dal [bot di EticaDigitale](https://gitlab.com/etica-digitale/antifloodbot) che però è scritto in Python e quindi non ho potuto *saccheggiarlo* ma ho dovuto riscriverlo da zero 😭

Il bot è infatti scritto in **PHP**.\
Più sotto alcuni miglioramenti che vorremo provare a fare. Se conosci il PHP e vuoi aiutarci apri una issue o PR con le modifiche o contattaci, grazie! 🧡

**COME CREARE UN PROPRIO BOT CON QUESTO CODICE**

1. Parlare in chat con BotFather su Telegram, premere ``/newbot`` e segnarsi l'API che vi verrà assegnata.
2. Sempre in chat con BotFather modificate il bot e abilitate l'opzione chiamata INLINE.
3. Inserite la vostra API nel file datisensibili.php al posto dellle XXXXXXXXXX" nella varabile ``$bot_id``.
4. Caricare tutti i file in una cartella del vostro web hosting, deve poter essere eseguito il codice PHP (praticamente tutti gli hosting lo permettono).
5. Segnatevi il nome dell'hosting che avete scelto e l'indirizzo del vostro file mybot.php. Facciamo finta sia: www.ilmiosito.it/mybot.php
6. Andate su questo indirizzo, modificando prima le scritte in maiuscolo, per abilitare il webhook: https://api.telegram.org/bot**IL-VOSTRO-TOKEN**/setwebhook?url=**WWW.ILMIOSITO.IT/MYBOT.PHP**
7. Ora il vostro bot è funzionante, all'interno di mybot.php trovate il "cuore" del bot, per modificare o cambiare le frasi modificate frasi.php. Quasi tutte le varie funzioni hanno un loro include di una pagina PHP dedicata.

**ABILITARE COMANDI PERSONALI**

Per abilitare i comandi personali di cui sopra codice all'interno del file mybot.php trovate 4 variabili:

- $skaid = XXXXX;
- $krustyid = XXXXX;

Queste identificano gli ID delle persone che possono usare i comandi di Codeberg: ``/new``, ``/bug``, ``/carte`` e ``/bot``.
Nel caso voleste abilitarli dovete anche inserire la vostra API di Codeberg (la trovate tra le impostazioni) mettendola nel file codeberg.php nella parte INSERIRE-QUI-API-CODEBERG. Ce ne sono quattro, una per ogni comando.

Oltre a Codeberg gli utenti selezionati possono anche utilizzare il comando ``/ot`` e il comando ``/delot``. Il primo fa solo scrivere al bot una frase (in questo caso di non andare OT). Il secondo cancella il messaggio quotato e lo inoltra automaticamente sul canale $lealternativechat dicendo che è stato inoltrato lì perché era OT.

- $lealternativegruppo = -1001244432641;
- $lealternativechat = -1001373077466;

Questi invece sono gli ID dei gruppi in cui deve essere abilitata la lista dei siti non affidabili. La lista dei siti non affidabili viene presa in automatico da blacklist.txt e va inserita una parola per ogni riga.

**MODIFICHE E AGGIORNAMENTI DA FARE**

[<mark>X</mark>] Attualmente non vengono riconosciuti i link all'interno delle entity ovvero non viene riconosciuto un link dentro una parola ([così](#)) e nemmeno se viene mandato un link nel testo di un'immagine o un video <mark>Fatto</mark>\
↳ [<mark>X</mark>] Nei gruppi per le *entities* al posto di rispondere ad ogni messaggio dovrebbe fare un messaggio solo e mettere i link uno dietro l'altro <mark>Fatto</mark>\
[<mark>X</mark>] Aggiungere il comando cerca per cercare direttamente all'interno del sito <mark>Fatto</mark>\
↳ [<mark>X</mark>] La ricerca con ``/search`` deve essere inline e mostrare i risultati inline direttamente dal sito lealternative.net utilizzando le API di ricerca di Wordpress\
[ ] Aggiungere altre novità e strumenti relativi alle alternative

# COME FUNZIONA IL BOT?

**CERCA INLINE**

scrivi @LeAlternativeBot in qualunque chat per evocare il bot e cercare il testo direttamente sul sito di LeAlternative.net.

**CONVERSIONE LINK IN PRIVATO**

Dopo aver avviato il bot scrivi all'interno della chat privata un link di ``YouTube | Twitter | Reddit | Medium | Imgur`` per un redirect automatico su front-end alternativi. Vengono riconosciuti i link dal testo, se inoltri un messaggio con dentro un link verrà estratto e convertito solo il link. Può convertire fino a 5 link contemporaneamente per dominio.
I front-end attualmente sono:

- **Youtube**: [Piped](https://piped.kavin.rocks), [Invidious Snopyta](https://invidious.snopyta.org) e [altre istanze](https://redirect.invidious.io), [ViewTube](https://viewtube.io/watch?v=NlUm63eAYJ8) e [CloudTube](https://tube.cadence.moe/watch?v=NlUm63eAYJ8).
- **Twitter**: [Nitter](https://twitit.gq/)
- **Reddit**: [Teddit](https://teddit.net/), [Libreddit](https://libredd.it/) e [Xeddit](https://xeddit.com/)
- **Medium**: [Scribe](https://scribe.rip/)
- **Imgur**: [Imgin](https://imgin.voidnet.tech/)

**CONVERSIONE LINK NEI GRUPPI**

Se invitato come amministratore all'interno di un gruppo risponderà a qualunque messaggio contenente un link di ``YouTube | Twitter | Reddit | Medium``. Risponderà suggerendo link alternativi a quelli inseriti nel messaggio originale.

**SROTOLAMENTO LINK ACCORCIATI**

Funziona sia in privato che nei gruppi: quando il bot vede un link accorciato dei siti: ``bit.ly | tinyurl.com | cutt.ly | kutt.it | lnk.pw | ow.ly | buff.ly | t.co`` svelerà il vero indirizzo di atterraggio dell'URL. 

## ALTRI COMANDI SECONDARI

``/start``
Avvia il bot e suggerisce come utilizzarlo

``/privacy``
Spiega come vengono trattati i dati dal bot. Il bot non tiene log né altro dunque si invita, principalmente, a guardare la policy di Telegram.

``/alternative``
Vengono fuori alcuni bottoni che invitano a esplorare tutto il mondo di LeAlternative.

``/istruzioni``
Alcune righe di istruzioni per far funzionare il bot correttamente.

--

**Licenza MIT**