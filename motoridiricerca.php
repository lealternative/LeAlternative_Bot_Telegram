<?php

$regexduckvuota = '%^[/.!]\bduck\b$%i';

if(preg_match($regexduckvuota,$text))  {
	
	$content = array(
		'chat_id' => $chat_id,
	  'text' => "<b>Ciao $name,</b> devi scrivere il comando <pre>/duck ricerca</pre>e non solamente /duck",
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}	

$regexduck = '%^[/.!]\bduck \b%i';

if(preg_match($regexduck,$text))  {
$searchquery = substr($text, 6);
	$url = "https://duckduckgo.com/?q=".$searchquery."&kp=-2&kl=it-it";
	$content = array(
		'chat_id' => $chat_id,
		'reply_to_message_id' => $replymessageid,
		'text' => "Lascia che il buon <b>DuckDuckGo</b> cerchi per te 👇",
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[
  ['text'=>'Cerca '.mb_strimwidth($searchquery, 0, 25, '...').' su DuckDuckGo','url' => $url],
]
	]]),
'parse_mode' => 'HTML',
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}

?>