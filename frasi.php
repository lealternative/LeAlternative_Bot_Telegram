<?php
if($msgType == 'inline_query'){
	
	if(!empty($inline_query_text)){
		
		$results = array(
			array(
				"type" => "article",
				"id" => "1",
				"title" => "Alternative a Google Search",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/10/18/alternative-a-google-search/&rhash=27fd4bba8b34fa) Come alternativa a Google consigliamo *DuckDuckGo*, *Swisscows* e *Startpage*. Leggi il nostro articolo: [Alternative a Google Search](https://www.lealternative.net/2019/10/18/alternative-a-google-search/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/10/18/alternative-a-google-search/",				
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/10/alternativeagooglesearch2.jpg.webp",
				"description" => "Motori di ricerca alternativi"
			  ),
			array(
				"type" => "article",
				"id" => "2",
				"title" => "Alternative a Google Chrome",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/10/21/alternative-a-google-chrome/&rhash=27fd4bba8b34fa) Come alternativa a Google Chrome su desktop consigliamo *Firefox* e *Brave*. Su Android meglio usare *Bromite*, *Firefox*, *Brave* oppure *DuckDuckGo Browser*. Su iOS potete provare *Firefox*, *DuckDuckGo Browser*, *Brave* e *SnowHaze*. Leggi il nostro articolo: [Alternative a Google Chrome](https://www.lealternative.net/2019/10/21/alternative-a-google-chrome/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/10/21/alternative-a-google-chrome/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/10/alternativeagooglechrome2.jpg.webp",
				"description" => "Browser alternativi"
			  ),
			array(
				"type" => "article",
				"id" => "3",
				"title" => "Alternative a YouTube",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/10/22/alternative-a-youtube/&rhash=27fd4bba8b34fa) Puoi vedere YouTube privatamente con *NewPipe* su Android. Puoi usare *FreeTube* su desktop e il sito *Invidious* sul web. Prova anche l'alternative decentralizzata a YouTube chiamata *PeerTube*! Leggi il nostro articolo: [Alternative a Youtube](https://www.lealternative.net/2019/10/22/alternative-a-youtube/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/10/22/alternative-a-youtube/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/10/alternativeayoutube2.jpg.webp",
				"description" => "YouTube e video sharing"
			  ),
			array(
				"type" => "article",
				"id" => "4",
				"title" => "Alternative a Google Translate",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/10/24/alternative-a-google-traduttore/&rhash=27fd4bba8b34fa) Non esiste solo Google Translate. Puoi provare *DeepL*, *LibreTranslate* e *Tatoeba*. Leggi il nostro articolo: [Alternative a Google Traduttore](https://www.lealternative.net/2019/10/24/alternative-a-google-traduttore/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/10/24/alternative-a-google-traduttore/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/10/alternativeagoogletraduttore2.jpg.webp",
				"description" => "Traduzioni"
			  ),		
			array(
				"type" => "article",
				"id" => "5",
				"title" => "Alternative a Google Maps",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/10/25/alternative-a-google-maps/&rhash=27fd4bba8b34fa) Prova qualcosa di diverso da Google Maps come *OsmAND*, *Magic Earth* oppure le mappe su *DuckDuckGo*. Per i mezzi pubblici pubblici prova *CityMapper* e *Moovit*. Per la bici? Prova *Naviki*! Leggi il nostro articolo: [Alternative a Google Maps](https://www.lealternative.net/2019/10/25/alternative-a-google-maps/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/10/25/alternative-a-google-maps/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/10/alternativeagooglemaps2.jpg.webp",
				"description" => "Mappe e viaggi"
			  ),
			array(
				"type" => "article",
				"id" => "6",
				"title" => "Alternative a Gmail",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/10/28/alternative-a-gmail/&rhash=27fd4bba8b34fa) Esistono email sicure e private come ad esempio *ProtonMail*, *Tutanota* oppure *Posteo*. Leggi il nostro articolo: [Alternative a Gmail](https://www.lealternative.net/2019/10/28/alternative-a-gmail/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/10/28/alternative-a-gmail/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/10/alternativeagmail2-1.jpg.webp",
				"description" => "Email"
			  ),
			array(
				"type" => "article",
				"id" => "7",
				"title" => "Alternative a Google Drive",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/10/29/alternative-a-google-drive/&rhash=27fd4bba8b34fa) Esistono tantissime alternative a Google Drive! Queste sono secondo noi le migliori: *Tresorit*, *Mega*, *Sync*, *Nextcloud*, *kDrive* oppure *Koofr*. Leggi il nostro articolo: [Alternative a Google Drive](https://www.lealternative.net/2019/10/29/alternative-a-google-drive/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/10/29/alternative-a-google-drive/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/10/alternativeagoogledrive2.jpg.webp",
				"description" => "File in cloud"
			  ),
			array(
				"type" => "article",
				"id" => "8",
				"title" => "Alternative a Google Password Manager",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/10/31/alternative-a-google-password-manager/&rhash=27fd4bba8b34fa) I password manager sono fondamentali per la sicurezza online. Prova *Bitwarden*, *Keepass* oppure *Firefox Lockwise*. Leggi il nostro articolo: [Alternative a Google Password Manager](https://www.lealternative.net/2019/10/31/alternative-a-google-password-manager/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/10/31/alternative-a-google-password-manager/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/10/alternativeagooglepassword2.jpg.webp",
				"description" => "Password al sicuro"
			  ),	
			array(
				"type" => "article",
				"id" => "9",
				"title" => "Alternative a Google Authenticator",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/11/01/alternative-a-google-authenticator/&rhash=27fd4bba8b34fa) Oltre a una password sicura è fondamentale l'autenticazione a due fattori. Quale applicazione utilizzare per la 2FA? Prova *Aegis* e *andOTP*! Leggi il nostro articolo: [Alternative a Google Authenticator](https://www.lealternative.net/2019/11/01/alternative-a-google-authenticator/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/11/01/alternative-a-google-authenticator/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/11/alternativeagoogleauthenticator2-1.jpg.webp",
				"description" => "Autenticazione a due fattori"
			  ),	
			array(
				"type" => "article",
				"id" => "10",
				"title" => "Alternative a Google Keep",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/11/04/alternative-a-google-keep/&rhash=27fd4bba8b34fa) Sono tante le applicazioni per prendere appunti in sicurezza. Prova *Standard Notes*, *Carnet*, *Simplenote* oppure *Joplin*. Leggi il nostro articolo: [Alternative a Google Keep](https://www.lealternative.net/2019/11/04/alternative-a-google-keep/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/11/04/alternative-a-google-keep/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/11/alternativeagooglekeep2.jpg.webp",
				"description" => "Prendere appunti e creare note"
			  ),
			array(
				"type" => "article",
				"id" => "11",
				"title" => "Alternative a GBoard",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2019/12/23/alternative-a-gboard/&rhash=27fd4bba8b34fa)Puoi provare una tastiera open source al posto di quella predefinita di Google e al posto di Microsoft SwiftKey. Cerca *Simple Keyboard*, *OpenBoard*, *AnySoftKeyboard* oppure *Hacker's Keyboard*. Leggi il nostro articolo: [Alternative a GBoard](https://www.lealternative.net/2019/12/23/alternative-a-gboard/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2019/12/23/alternative-a-gboard/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2019/12/alternativeagboard2.jpg.webp",
				"description" => "Tastiere Android"
			  ),
			array(
				"type" => "article",
				"id" => "12",
				"title" => "Alternative a Whatsapp",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/01/09/alternative-a-whatsapp/&rhash=27fd4bba8b34fa) Ci sono molte alternative interessanti a Whatsapp. Parliamo di *Signal*, *Threema* ma anche di *Element* che utilizza il protocollo *Matrix*. E ovviamente *Telegram*. Leggi il nostro articolo: [Alternative a Whatsapp](https://www.lealternative.net/2020/01/09/alternative-a-whatsapp/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/01/09/alternative-a-whatsapp/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/alternativeawhatsapp2-1.jpg.webp",
				"description" => "Messaggistica istantanea"
			  ),
			array(
				"type" => "article",
				"id" => "13",
				"title" => "Alternative a Google Messaggi",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/01/14/alternative-a-google-messaggi/&rhash=27fd4bba8b34fa) Anche per leggere gli SMS ci sono alternative open source valide. Prova *Signal*, *QKSMS* e *Simple SMS Messenger*. Leggi il nostro articolo: [Alternative a Google Messaggi](https://www.lealternative.net/2020/01/14/alternative-a-google-messaggi/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/01/14/alternative-a-google-messaggi/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/alternativeagooglemessaggi.jpg.webp",
				"description" => "SMS"
			  ),		
			array(
				"type" => "article",
				"id" => "14",
				"title" => "Alternative a Google Photos",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/01/22/alternative-a-google-photos/&rhash=27fd4bba8b34fa) Come archiviare le proprie foto online senza Google Photo? Prova *Piwigo*, *Cryptee*, *Jottacloud* e anche *Stingle Photos*. Leggi il nostro articolo: [Alternative a Google Photos](https://www.lealternative.net/2020/01/22/alternative-a-google-photos/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/01/22/alternative-a-google-photos/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/alternativeagooglephotos2.jpg.webp",
				"description" => "Fotografie in cloud"
			  ),
			array(
				"type" => "article",
				"id" => "15",
				"title" => "Alternative a Google News",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/01/29/alternative-a-google-news/&rhash=27fd4bba8b34fa) Per leggere le notizie e rimanere aggiornati noi consigliamo di utilizzare gli *RSS* con programmi come *Newsblur* o *Flym News Reader*. Leggi il nostro articolo: [Alternative a Google News](https://www.lealternative.net/2020/01/29/alternative-a-google-news/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/01/29/alternative-a-google-news/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/alternativeagooglenews.jpg.webp",
				"description" => "Notizie e aggiornamenti"
			  ),
			array(
				"type" => "article",
				"id" => "16",
				"title" => "Alternative a Microsoft Office",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/02/05/alternative-a-microsoft-office/&rhash=27fd4bba8b34fa) Sapevi che OpenOffice non viene più aggiornato da quasi 10 anni? Prova a utilizzare *LibreOffice*. Leggi il nostro articolo: [Alternative a Microsoft Office](https://www.lealternative.net/2020/02/05/alternative-a-microsoft-office/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/02/05/alternative-a-microsoft-office/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/microsoftoffice.jpg.webp",
				"description" => "Documenti e fogli di calcolo"
			  ),
			array(
				"type" => "article",
				"id" => "17",
				"title" => "Alternative a Google Calendar",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/02/19/alternative-a-google-calendar/&rhash=27fd4bba8b34fa) Tante le alternative per gestire i calendari, da *Tutanota* a *Fruux* passando per *Nextcloud* e *Kolab Now*. Leggi il nostro articolo: [Alternative a Google Calendar](https://www.lealternative.net/2020/02/19/alternative-a-google-calendar/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/02/19/alternative-a-google-calendar/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/02/calendario.jpg.webp",
				"description" => "Calendario"
			  ),
			array(
				"type" => "article",
				"id" => "18",
				"title" => "Alternative a Twitter",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/03/04/alternative-a-twitter/&rhash=27fd4bba8b34fa) La vera alternativa a Twitter si chiama *Mastodon*. In alternativa puoi leggere Twitter privatamente con *Nitter* e *Twidere*. Leggi il nostro articolo: [Alternative a Twitter](https://www.lealternative.net/2020/03/04/alternative-a-twitter/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/03/04/alternative-a-twitter/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/tweet.jpg.webp",
				"description" => "Social network"
			  ),
			array(
				"type" => "article",
				"id" => "19",
				"title" => "Alternativa a Google MyMaps",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/03/11/umap/&rhash=27fd4bba8b34fa) Abbiamo davvero un'alternativa per tutto, anche per Google MyMaps. Cercate *uMap* e non ne rimarrete delusi. Leggi il nostro articolo: [uMap](https://www.lealternative.net/2020/03/11/umap/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/03/11/umap/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/umap.jpg.webp",
				"description" => "Mappe personalizzate"
			  ),
			array(
				"type" => "article",
				"id" => "20",
				"title" => "Alternative ad Android",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/03/25/alternative-ad-android/&rhash=27fd4bba8b34fa) Prova il cellulare della *e Foundation* oppure *iodé*. C'è anche il *Fairphone* per avere un Android sostenibile. Leggi il nostro articolo: [Alternative ad Android](https://www.lealternative.net/2020/03/25/alternative-ad-android/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/03/25/alternative-ad-android/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/android.jpg.webp",
				"description" => "Smartphone Android"
			  ),
			array(
				"type" => "article",
				"id" => "21",
				"title" => "Alternative a Zoom",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/04/06/quale-alternativa-a-zoom/&rhash=27fd4bba8b34fa) Al posto di Zoom prova *Jitsi Meet* e il sito iorestoacasa.work. C'è anche *Together Brave* e *Infomaniak Meet*. Leggi il nostro articolo: [Alternative a Zoom](https://www.lealternative.net/2020/04/06/quale-alternativa-a-zoom/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/04/06/quale-alternativa-a-zoom/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/04/damir-kopezhanov-VM1Voswbs0A-unsplash.jpg.webp",
				"description" => "Videochiamate"
			  ),
			array(
				"type" => "article",
				"id" => "22",
				"title" => "Alternative a Facebook",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/04/08/alternative-a-facebook/&rhash=27fd4bba8b34fa) Sconsigliamo l'uso di Facebook, al suo posto potreste usare *Friendica*. Tuttavia per leggere Facebook più privatamente potete provare l'app *Frost*. Leggi il nostro articolo: [Alternative a Facebook](https://www.lealternative.net/2020/04/08/alternative-a-facebook/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/04/08/alternative-a-facebook/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/01/occhif.jpg.webp",
				"description" => "Social network"
			  ),
			array(
				"type" => "article",
				"id" => "23",
				"title" => "Leggere Reddit",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/04/10/slide/&rhash=27fd4bba8b34fa) Per leggere Reddit non avete bisogno dell'app ufficiale: provate a installare *Slide*. Leggi il nostro articolo: [Slide](https://www.lealternative.net/2020/04/10/slide/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/04/10/slide/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/03/slide.jpg.webp",
				"description" => "Social network"
			  ),
			array(
				"type" => "article",
				"id" => "24",
				"title" => "Alternative a Google Analytics",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/04/22/alternative-a-google-analytics/&rhash=27fd4bba8b34fa) Non riempire il web di spazzatura usando Google Analytics! Prova *GoatCounter*, *Matomo*, *Koko Analytics* oppure *Statify*. Leggi il nostro articolo: [Alternative a Google Analytics](https://www.lealternative.net/2020/04/22/alternative-a-google-analytics/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/04/22/alternative-a-google-analytics/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/02/analytics-text-185576.jpg.webp",
				"description" => "Statistiche siti web"
			  ),
			array(
				"type" => "article",
				"id" => "25",
				"title" => "Alternative ai DNS di Google",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/04/29/alternative-ai-dns-di-google/&rhash=27fd4bba8b34fa) Al posto dei DNS di Google prova ad usare *AdGuard*, *Cloudflare*, *BlahDNS*, *NextDNS*, *AhaDNS* oppure *Snopyta*. Leggi il nostro articolo: [Alternative ai DNS di Google](https://www.lealternative.net/2020/04/29/alternative-ai-dns-di-google/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/04/29/alternative-ai-dns-di-google/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/04/dns.jpg.webp",
				"description" => "DNS"
			  ),
			array(
				"type" => "article",
				"id" => "26",
				"title" => "Alternative a Spotify",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/05/06/alternative-a-spotify/&rhash=27fd4bba8b34fa) Per non essere tracciato da Spotify prova un'alternativa come *Funkwhale*, *Libre.fm*, *NewPipe*, *eSound Music* o *Radio4000*. Leggi il nostro articolo: [Alternative a Spotify](https://www.lealternative.net/2020/05/06/alternative-a-spotify/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/05/06/alternative-a-spotify/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/04/spot.jpg.webp",
				"description" => "Musica"
			  ),
			array(
				"type" => "article",
				"id" => "27",
				"title" => "Alternative a WeTransfer",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/05/13/alternative-a-wetransfer/&rhash=27fd4bba8b34fa) Sconsigliamo l'uso di WeTransfer per trasferire file. Prova invece *Jirafeau*, *Lufi*, *Framadrop*, *FileSend* oppure *SwissTransfer*. Leggi il nostro articolo: [Alternative a WeTransfer](https://www.lealternative.net/2020/05/13/alternative-a-wetransfer/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/05/13/alternative-a-wetransfer/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/04/upload-3406226_1920.jpg.webp",
				"description" => "Trasferimento file"
			  ),
			array(
				"type" => "article",
				"id" => "28",
				"title" => "Alternative a Eventbrite",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/06/24/alternative-ad-eventbrite/&rhash=27fd4bba8b34fa) State lontani da Eventbrite o dagli eventi di Facebook! La migliore applicazioni per gestire gli eventi secondo noi è *Mobilizon*. Leggi il nostro articolo: [Alternative a Eventbrite](https://www.lealternative.net/2020/06/24/alternative-ad-eventbrite/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/06/24/alternative-ad-eventbrite/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/06/seyedeh-hamideh-kazemi-PFUqfNsorJM-unsplash.jpg.webp",
				"description" => "Eventi e feste"
			  ),
			array(
				"type" => "article",
				"id" => "29",
				"title" => "Alternative ad AirBnB",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/07/17/alternative-ad-airbnb/&rhash=27fd4bba8b34fa) Cosa usare al posto di AirBnB? Provate *Fairbnb*, *BeWelcome*, *Sharewood Adventure* oppure *Addiopizzo Travel*. Leggi il nostro articolo: [Alternative ad AirBnB](https://www.lealternative.net/2020/07/17/alternative-ad-airbnb/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/07/17/alternative-ad-airbnb/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/07/maxime-roedel-A4Emn6JMU10-unsplash2.jpg.webp",
				"description" => "Vacanze e alberghi"
			  ),
			array(
				"type" => "article",
				"id" => "30",
				"title" => "OpenContacts",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/07/27/opencontacts/&rhash=27fd4bba8b34fa) Grazie a OpenContacts potete mettere in sicurezza i contatti della vostra rubrica. Leggi il nostro articolo: [OpenContacts](https://www.lealternative.net/2020/07/27/opencontacts/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/07/27/opencontacts/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/04/cellular-2866230_1920.jpg.webp",
				"description" => "Contatti e rubrica"
			  ),
			array(
				"type" => "article",
				"id" => "31",
				"title" => "Alternative a Instagram",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/09/04/alternative-ad-instagram/&rhash=27fd4bba8b34fa) L'alternativa decentralizzata a Instagram si chiama *PixelFed*. Altrimenti potete guarda Instagram privatamente grazie al progetto *Bibliogram* e all'app *Barinsta*. Leggi il nostro articolo: [Alternative a Instagram](https://www.lealternative.net/2020/09/04/alternative-ad-instagram/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/09/04/alternative-ad-instagram/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/09/lalo-hernandez-r34cKhbEDCU-unsplash.jpg.webp",
				"description" => "Social network"
			  ),
			array(
				"type" => "article",
				"id" => "32",
				"title" => "Accorciare gli URL",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/10/05/anonymous-url-shortener/&rhash=27fd4bba8b34fa) Per accorciare gli URL potete provare *s.devol.it*, *Anonymous URL Shortener* oppure *Kutt*. Leggi il nostro articolo: [Accorciare gli URL](https://www.lealternative.net/2020/10/05/anonymous-url-shortener/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/10/05/anonymous-url-shortener/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/09/undraw_security_o890.png.webp",
				"description" => "Strumenti quotidiani"
			  ),
			array(
				"type" => "article",
				"id" => "33",
				"title" => "Alternative ad Amazon",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/10/12/alternative-ad-amazon/&rhash=27fd4bba8b34fa) Crediamo sia giusto anche comprare meno da Amazon. Alcune alternative sono *Etsy* per i regali e *Bookdealer* per i libri. Leggi il nostro articolo: [Alternative ad Amazon](https://www.lealternative.net/2020/10/12/alternative-ad-amazon/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/10/12/alternative-ad-amazon/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/10/online-shopping-4532460_1920.jpg.webp",
				"description" => "Acquisti online"
			  ),
			array(
				"type" => "article",
				"id" => "34",
				"title" => "Alternative a Skype",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/10/16/alternative-a-skype/&rhash=27fd4bba8b34fa) Al posto di Skype provate alternative come *Jami*, *Wire*, *Telegram*, *Signal*, *Tox* oppure *Jitsi*. Leggi il nostro articolo: [Alternative a Skype](https://www.lealternative.net/2020/10/16/alternative-a-skype/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/10/16/alternative-a-skype/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/10/skype.png.webp",
				"description" => "Videochiamate di gruppo"
			  ),
			array(
				"type" => "article",
				"id" => "35",
				"title" => "Alternative a Google Forms",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/10/28/alternative-a-google-forms/&rhash=27fd4bba8b34fa) Prova ad utilizzare *JotForms*, *LimeSurvey*, *Webform* oppure *Framaforms*. Leggi il nostro articolo: [Alternative a Google Forms](https://www.lealternative.net/2020/10/28/alternative-a-google-forms/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/10/28/alternative-a-google-forms/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/10/kelly-sikkema-SkFdmKGxQ44-unsplash.jpg.webp",
				"description" => "Sondaggi online"
			  ),
			array(
				"type" => "article",
				"id" => "36",
				"title" => "Alternative a GitHub",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/11/13/alternative-a-github/&rhash=27fd4bba8b34fa) Prova a mettere il tuo codice su *Gitea*, *Codeberg*, *GitLab* oppure *Gogs*. Leggi il nostro articolo: [Alternative a GitHub](https://www.lealternative.net/2020/11/13/alternative-a-github/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/11/13/alternative-a-github/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/11/martin-w-kirst-Ap3fFS0iOiE-unsplash.jpg.webp",
				"description" => "Hosting codici sorgente"
			  ),
			array(
				"type" => "article",
				"id" => "37",
				"title" => "Alternative a Blogger",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/11/17/creare-un-blog-alternative-a-blogger/&rhash=27fd4bba8b34fa) Per creare un blog prova ad utilizzare *NoBlogo*, *Bear* *Write.as* oppure *Telegra.ph* e *Rentry*! Leggi il nostro articolo: [Alternative a Blogger](https://www.lealternative.net/2020/11/17/creare-un-blog-alternative-a-blogger/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/11/17/creare-un-blog-alternative-a-blogger/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/11/retrosupply-jLwVAUtLOAQ-unsplash.jpg.webp",
				"description" => "Blog personale"
			  ),
			array(
				"type" => "article",
				"id" => "38",
				"title" => "Alternativa a Microsoft Teams",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/11/27/alternativa-a-microsoft-teams/&rhash=27fd4bba8b34fa) Prova a utilizzare l'applicazione [Twake](https://twake.app/)! Leggi il nostro articolo: [Alternativa a Microsoft Teams](https://www.lealternative.net/2020/11/27/alternativa-a-microsoft-teams/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/11/27/alternativa-a-microsoft-teams/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/11/you-x-ventures-Oalh2MojUuk-unsplash.jpg.webp",
				"description" => "Workspace"
			  ),
			array(
				"type" => "article",
				"id" => "39",
				"title" => "Jam, l'alternativa a Clubhouse",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2021/02/17/jam-lalternativa-open-source-a-clubhouse/&rhash=27fd4bba8b34fa) Al posto di iscriverti a Clubhouse prova a usare un'alternativa gratuita e libera come [Jam](https://jam.systems/)! Leggi il nostro articolo: [Jam, l'alternativa a Clubhouse](https://www.lealternative.net/2021/02/17/jam-lalternativa-open-source-a-clubhouse/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2021/02/17/jam-lalternativa-open-source-a-clubhouse/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2021/02/keji-gao-yHI9CyBZlfE-unsplash-1200x750.jpg.webp",
				"description" => "Social network audio"
			  ),
			array(
				"type" => "article",
				"id" => "40",
				"title" => "App per leggere Reddit e alcune alternative",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2021/03/03/app-per-leggere-reddit-e-alcune-alternative/&rhash=27fd4bba8b34fa) Per leggere Reddit puoi usare app open source come Slide, Infinity o Dawn for Reddit. In alternativa a Reddit puoi provare Lemmy, Gambe.ro e Tildes. Leggi il nostro articolo: [App per leggere Reddit e alcune alternative](https://www.lealternative.net/2021/03/03/app-per-leggere-reddit-e-alcune-alternative/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2021/03/03/app-per-leggere-reddit-e-alcune-alternative/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2021/02/pexels-brett-jordan-5437588.jpg",
				"description" => "Social network"
			  ),
			array(
				"type" => "article",
				"id" => "41",
				"title" => "BleachBit, l’alternativa a CCleaner",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2021/03/15/bleachbit-lalternativa-a-ccleaner/&rhash=27fd4bba8b34fa) Al posto di utilizzare CCleaner (di proprietà di Avast) prova il software open source BleachBit! Leggi il nostro articolo: [BleachBit, l’alternativa a CCleaner](https://www.lealternative.net/2021/03/15/bleachbit-lalternativa-a-ccleaner/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2021/03/15/bleachbit-lalternativa-a-ccleaner/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2021/03/oliver-hale-oTvU7Zmteic-unsplash-1.jpg.webp",
				"description" => "Pulizia computer"
			  ),
			array(
				"type" => "article",
				"id" => "42",
				"title" => "Alternative a Windows",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2020/10/20/come-iniziare-ad-usare-linux/&rhash=27fd4bba8b34fa) Come alternativa a Windows suggeriamo di utilizzare Linux. Anche per i principianti, prova a leggere il nostro articolo: [Come iniziare ad usare Linux](https://www.lealternative.net/2020/10/20/come-iniziare-ad-usare-linux/)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2020/10/20/come-iniziare-ad-usare-linux/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2020/04/linux-1962898.jpg.webp",
				"description" => "Computer desktop"
			  ),
			array(
				"type" => "article",
				"id" => "43",
				"title" => "Alternative a Discord",
				"message_text" => "[💡](https://t.me/iv?url=https://www.lealternative.net/2021/03/26/alternative-a-discord/&rhash=27fd4bba8b34fa) Quali alternative a Discord esistono? Nessuna è uguale a Discord ma si può provare ad utilizzare Mumble, Telegram, Matrix o Guilded. Leggi il nostro articolo: [Alternative a Discord](https://www.lealternative.net/2021/03/26/alternative-a-discord)",
			  	"parse_mode" => "markdown",
				"url" => "https://www.lealternative.net/2021/03/26/alternative-a-discord/",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2021/03/fredrick-tendong-6ou8gWpS9ns-unsplash.jpg.webp",
				"description" => "Chat"
			  )	
		);
		
		// INIZIO MODIFICHE FRANCESCO
		//definiamo un nuovo array vuoto
		$resultfiltered = [];
		
		// Guida veloce array PHP: https://www.selectallfromdual.com/blog/5013/gli-array-in-php
		// Ciclo l'array $result e inserisco il valore di ogni elemento in $value
		foreach ($results as $value) {
		
			//verifica se nel title dell'elemento dell'array è contenuto il valore che abbiamo inserito
			if (strpos(strtoupper($value["title"]), strtoupper($inline_query_text)) !== false
or
strpos(strtoupper($value["description"]), strtoupper($inline_query_text)) !== false
or
strpos(strtoupper($value["message_text"]), strtoupper($inline_query_text)) !== false
) {
				//se la nostra $inline_query_text è contenuta nel title del singolo elemento dell'array, allora inseriamo $value nel nostro nuovo array
				array_push($resultfiltered, $value);
			}
		
		}
		
		
		//$results = json_encode($results);
		$results = json_encode($resultfiltered);
		
		// così ad occhio dovrebbe funzionare :)
		// FINE MODIFICHE FRANCESCO
		
		$zeroresult = [];
if (sizeof($resultfiltered) == 0) {
	
$zeroresult = array(
			array(
				"type" => "article",
				"id" => "1",
				"title" => "Nessun risultato trovato!",
				"message_text" => "Mi dispiace ma non ho trovato nessun risultato rapido 😿 Prova a fare la tua domanda sul gruppo @LeAlternativeGruppoUfficiale!\n
Oppure scrivi direttamente al bot \"cerca <i>testodacercare</i>\", potremmo averne già parlato sul sito!",
			  	"parse_mode" => "HTML",
				"url" => "https://t.me/LeAlternativeGruppoUfficiale",
				"thumb_url" => "https://www.lealternative.net/wp-content/uploads/2021/02/undraw_Faq_re_31cw-1.png.webp",
				"description" => "Prova a chiedere sul nostro gruppo",
				'disable_web_page_preview' => true
			  )
		);
		
		$results = json_encode($zeroresult);
	
}
		

		$content = array('inline_query_id' => $inline_query_id, "results" => $results);
		$telegram->answerInlineQuery($content);		
	}
	
}
?>