<?php

$regexistruzioni = '%^[/.!]\bistruzioni@lealternativebot\b$%i';

if (($messagefromagroup === TRUE) AND preg_match($regexistruzioni,$text))
	{
	$content = array(
		'chat_id' => $chat_id,
		"text" => "ciao, utilizza il comando `/istruzioni` in privato, grazie!",
'parse_mode' => 'MARKDOWN'
	);
	$telegram->sendMessage($content);
	}

if (($messagefromagroup === FALSE) AND $text == '/istruzioni')
	{
	$content = array(
		'chat_id' => $chat_id,
		'text' => "Posso fare molte cose:\n\n• <b>puoi usarmi inline</b>;\n• <b>ricerca sul sito LeAlternative:</b>;\n• <b>front-end alternativi in privato e in gruppi</b>;\n• <b>srotolo URL</b>;\n• <b>traduzioni con <a href=\"https://github.com/TheDavidDelta/lingva-translate\">Lingva Translate</a></b>;\n• <b>ricerca su DuckDuckGo</b>.\n\nClicca sui bottoni qui sotto per leggere le singole istruzioni 👇",
'parse_mode' => 'HTML',
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[ // row #1
  ['text'=>'Ricerca inline', 'callback_data' => 'istruzioni-inline'], // row #1, button #1
  ['text'=>'Ricerca sul sito', 'callback_data' => 'istruzioni-ricerca'],  // row #1, button #2
  ['text'=>'Traduzioni', 'callback_data' => 'istruzioni-traduzioni']  // row #1, button #2
],
[ // row #2
  ['text'=>'Front-end alternativi', 'callback_data' => 'istruzioni-front-end'], // row #2, button #1
['text'=>'Srotolo URL', 'callback_data' => 'istruzioni-srotolo'], // row #2, button #1  
['text'=>'Ricerca DuckDuckGo', 'callback_data' => 'istruzioni-duckduckgo'] // row #2, button #1  
]
	]]),
'disable_web_page_preview' => true
	);
	$telegram->sendMessage($content);
	}
	
if ($callback_query_data == 'istruzioni')		{
	$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
		'text' => "Posso fare molte cose:\n\n• <b>puoi usarmi inline</b>;\n• <b>ricerca sul sito LeAlternative:</b>;\n• <b>front-end alternativi in privato e in gruppi</b>;\n• <b>srotolo URL</b>;\n• <b>traduzioni con <a href=\"https://github.com/TheDavidDelta/lingva-translate\">Lingva Translate</a></b>;\n• <b>ricerca su DuckDuckGo</b>.\n\nClicca sui bottoni qui sotto per leggere le singole istruzioni 👇",
'parse_mode' => 'HTML',
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[ // row #1
  ['text'=>'Ricerca inline', 'callback_data' => 'istruzioni-inline'], // row #1, button #1
  ['text'=>'Ricerca sul sito', 'callback_data' => 'istruzioni-ricerca'],  // row #1, button #2
  ['text'=>'Traduzioni', 'callback_data' => 'istruzioni-traduzioni']  // row #1, button #2
],
[ // row #2
  ['text'=>'Front-end alternativi', 'callback_data' => 'istruzioni-front-end'], // row #2, button #1
['text'=>'Srotolo URL', 'callback_data' => 'istruzioni-srotolo'], // row #2, button #1  
['text'=>'Ricerca DuckDuckGo', 'callback_data' => 'istruzioni-duckduckgo'] // row #2, button #1  
]
	]]),
'disable_web_page_preview' => true
	);
	$telegram->editMessageText($content);
	
	$content2 = array(
		'callback_query_id' => $callback_query_id
		);
	$telegram->answerCallbackQuery($content2);
	}

if ($callback_query_data == 'istruzioni-front-end')	{
$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
  'text' => "<b>Front-end alternativi in privato</b>:\nscrivi all'interno di questa chat un link di: <pre>YouTube | Twitter | Reddit | Medium | Imgur | TikTok</pre> per un redirect automatico su front-end alternativi! Posso reindirizzare fino a 5 link contemporaneamente dello stesso dominio;\n\n<b>Front-end alternativi in gruppi</b>:\ninvitami come amministratore nel tuo gruppo. Quando vedrò link di <pre>YouTube | Reddit | Twitter | Medium | TikTok</pre> suggerirò nel gruppo i front-end alternativi per quei link!.\n\nRiesco a leggere sia i link normali che quelli inseriti dentro il testo tipo <a href=\"https://www.lealternative.net\">così</a>.",
'parse_mode' => 'HTML',
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[
  ['text'=>'Torna alle istruzioni','callback_data' => 'istruzioni'],
]
	]]),
'disable_web_page_preview' => true
	);
	$telegram->editMessageText($content);
	
	$content2 = array(
		'callback_query_id' => $callback_query_id
		);
	$telegram->answerCallbackQuery($content2);	
}

if ($callback_query_data == 'istruzioni-ricerca')	{
$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
  'text' => "<b>Ricerca su LeAlternative</b>:\nscrivendo le parole <b>cerca</b> <i>nome alternative</i> ti appariranno i primi tre risultati del sito <a href=\"https://www.lealternative.net\">LeAlternative.net</a>!\n\n<b>Ad esempio</b> prova a scrivere: <pre>cerca Alternative a WhatsApp</pre>",
'parse_mode' => 'HTML',
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[
  ['text'=>'Torna alle istruzioni','callback_data' => 'istruzioni'],
]
	]]),
'disable_web_page_preview' => true
	);
	$telegram->editMessageText($content);
	
	$content2 = array(
		'callback_query_id' => $callback_query_id
		);
	$telegram->answerCallbackQuery($content2);	

}

if ($callback_query_data == 'istruzioni-traduzioni')	{
$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
  'text' => "<b>Traduzioni con <a href=\"https://github.com/TheDavidDelta/lingva-translate\">Lingva Translate</a></b>:\nPuoi tradurre qualunque testo scrivendo <pre>/trad testodatradurre</pre> oppure, all'interno di un gruppo dove il bot è amministratore puoi rispondere a un messaggio con il comando <pre>/trad</pre> e lui lo risponderà con la traduzione.",
'parse_mode' => 'HTML',
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[
  ['text'=>'Torna alle istruzioni','callback_data' => 'istruzioni'],
]
	]]),
'disable_web_page_preview' => true
	);
	$telegram->editMessageText($content);
	
	$content2 = array(
		'callback_query_id' => $callback_query_id
		);
	$telegram->answerCallbackQuery($content2);		
	
}

if ($callback_query_data == 'istruzioni-srotolo')	{
$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
  'text' => "<b>Srotolo URL in privato e nei gruppi</b>:\nse mi mandi un messaggio da un indirizzo accorciato dai siti: <pre>bit.ly | tinyurl.com | cutt.ly | kutt.it | lnk.pw | ow.ly | buff.ly | t.co</pre> posso dirti quale è il vero indirizzo sul quale verrai portato così da evitare il tracciamento da parte di questi siti. Sono utile anche per la tua sicurezza: dietro alcuni link accorciati potrebbero nascondersi link fraudolenti!",
'parse_mode' => 'HTML',
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[
  ['text'=>'Torna alle istruzioni','callback_data' => 'istruzioni'],
]
	]]),
'disable_web_page_preview' => true
	);
	$telegram->editMessageText($content);

	$content2 = array(
		'callback_query_id' => $callback_query_id
		);
	$telegram->answerCallbackQuery($content2);		
	
}

if ($callback_query_data == 'istruzioni-inline')	{
$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
  'text' => "<b>Utilizzo inline</b>:\nscrivi @LeAlternativeBot <i>frase</i> in qualunque chat per evocarmi e cercare alternative rapide. Queste alternative sono aggiornate periodicamente e fanno parte di una lista preimpostata non viene dunque ricercato all'interno del sito.\nÈ molto utile soprattutto per mandare a qualcuno o qualcuna il link diretto in chat.\n\n<b>Ad esempio</b> prova a scrivere: <pre>@LeAlternativeBot WhatsApp</pre>",
'parse_mode' => 'HTML',
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[
  ['text'=>'Torna alle istruzioni','callback_data' => 'istruzioni'],
]
	]]),
'disable_web_page_preview' => true
	);
	$telegram->editMessageText($content);
	
	$content2 = array(
		'callback_query_id' => $callback_query_id
		);
	$telegram->answerCallbackQuery($content2);		
	
}

if ($callback_query_data == 'istruzioni-duckduckgo')	{
$content = array(
		'chat_id' => $chat_id,
		'message_id' => $message_id,
  'text' => "<b>Ricerca su DuckDucKGo</b>:\nCrea un pulsante per cercare automaticamente del testo sul motore di ricerca alterativo <b>DuckDuckGo</b>. Scrivi soltanto <pre>/duck testodacercare</pre> e verrà creato un bottone che, premuto, cercherà quel testo su DuckDuckGo.",
'parse_mode' => 'HTML',
'reply_markup' =>json_encode([
'inline_keyboard'=>[
[
  ['text'=>'Torna alle istruzioni','callback_data' => 'istruzioni'],
]
	]]),
'disable_web_page_preview' => true
	);
	$telegram->editMessageText($content);

	$content2 = array(
		'callback_query_id' => $callback_query_id
		);
	$telegram->answerCallbackQuery($content2);		
	
}

?>